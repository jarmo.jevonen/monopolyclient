package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsPropertyRemovePropertyOwnershipTest {
    private DTOafsPropertyRemovePropertyOwnership dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsPropertyRemovePropertyOwnership();
        dto.setPlayerId("FooId");
        dto.setPropertyIndices(List.of(1, 2, 3));
    }

    @Test
    void getPropertyIndices() {
        assertEquals(List.of(1, 2, 3), dto.getPropertyIndices());
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}