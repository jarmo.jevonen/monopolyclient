package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsGameStatePlayerWentBankruptTest {
    private DTOafsGameStatePlayerWentBankrupt dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsGameStatePlayerWentBankrupt();
        dto.setPlayerId("FooId");
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}