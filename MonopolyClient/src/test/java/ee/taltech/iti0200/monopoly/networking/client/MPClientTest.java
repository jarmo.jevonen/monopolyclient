package ee.taltech.iti0200.monopoly.networking.client;

import com.esotericsoftware.kryonet.Client;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class MPClientTest {
    private MPClient mpClient;
    private GameNetworkingClientLogic gameNetworkingClientLogic;

    @BeforeEach
    void setUp() {
        gameNetworkingClientLogic = new GameNetworkingClientLogic(null);
        mpClient = new MPClient("localhost", 25565, "Alice", gameNetworkingClientLogic, "", false);
    }

    @Test
    void getUserName() {
        assertEquals("Alice", mpClient.getUserName());
    }

    @Test
    void getPlayerId() {
        assertEquals("", mpClient.getPlayerId());
    }

    @Test
    void getClientLogger() {
        Logger logger = MPClient.getClientLogger();
        assertEquals(logger, MPClient.getClientLogger());
    }

    @Test
    void getClient() {
        Client client = mpClient.getClient();
        assertEquals(client, mpClient.getClient());
    }

    @Test
    void getGameNetworkingClientLogic() {
        assertEquals(gameNetworkingClientLogic, mpClient.getGameNetworkingClientLogic());
    }

    @Test
    void assignPlayerId() {
        mpClient.assignPlayerId("");
        assertEquals("", mpClient.getPlayerId());
    }
}