package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver;

import ee.taltech.iti0200.monopoly.game.card.CardType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsCardTakeACardTest {
    private DTOafsCardTakeACard dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsCardTakeACard();
        dto.setPlayerId("FooId");
        dto.setCardType(CardType.CHANCE);
        dto.setCardIndex(5);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void getCardType() {
        assertEquals(CardType.CHANCE, dto.getCardType());
    }

    @Test
    void getCardIndex() {
        assertEquals(5, dto.getCardIndex());
    }
}