package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsGameStatePlayerBankruptcyAcknowledgementTest {
    private DTOatsGameStatePlayerBankruptcyAcknowledgement dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsGameStatePlayerBankruptcyAcknowledgement();
        dto.setPlayerId("Foo");
    }

    @Test
    void getPlayerId() {
        assertEquals("Foo", dto.getPlayerId());
    }
}