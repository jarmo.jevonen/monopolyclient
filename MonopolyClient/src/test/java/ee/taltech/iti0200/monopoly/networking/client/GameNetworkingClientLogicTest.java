package ee.taltech.iti0200.monopoly.networking.client;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameNetworkingClientLogicTest {
    private GameNetworkingClientLogic gameNetworkingClientLogic;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        gameNetworkingClientLogic = new GameNetworkingClientLogic(null);
        objectMapper = new ObjectMapper();
        gameNetworkingClientLogic.setDtoGameDTOHandler(null);
        gameNetworkingClientLogic.setObjectMapper(objectMapper);
    }

    @Test
    void connectToServerCreate() {
        gameNetworkingClientLogic.connectToServer("localhost", 25565, "Alice", "", false);
    }

    @Test
    void connectToServerJoin() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.connectToServer("localhost", 25565, "Alice", "123", true));
    }

    @Test
    void sendAddAIPlayer() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendAddAIPlayer());
    }

    @Test
    void sendReadyStatus() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendReadyStatus(false));
    }

    @Test
    void sendCardChoice() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendCardChoice(false));
    }

    @Test
    void sendCardUseGetOutOfJailCard() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendCardUseGetOutOfJailCard(false));
    }

    @Test
    void sendDieRollTheDice() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendDieRollTheDice());
    }

    @Test
    void sendGameStatePlayerEndTurn() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendGameStatePlayerEndTurn());
    }

    @Test
    void sendMoneyPayToGetOutOfJail() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendMoneyPayToGetOutOfJail());
    }

    @Test
    void sendPropertyPurchaseDecision() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendPropertyPurchaseDecision(false));
    }

    @Test
    void sendPropertyUpDowngradeDecision() {
        assertThrows(NullPointerException.class, () -> gameNetworkingClientLogic.sendPropertyUpDowngradeDecision(8, false));
    }

    @Test
    void receiveStartTheGame() {
    }

    @Test
    void receiveClientDisconnected() {
    }

    @Test
    void receiveGameDataPlayers() {
    }

    @Test
    void receiveGameDataTiles() {
    }

    @Test
    void receiveGameDataCardsChance() {
    }

    @Test
    void receiveGameDataCardsCommunityChest() {
    }

    @Test
    void receivePlayerReadyStatuses() {
    }

    @Test
    void receiveCardOfferSwitchACardOrPay() {
    }

    @Test
    void receiveCardSetPlayerOwnedGetOutOfJailCardsCount() {
    }

    @Test
    void receiveCardTakeACard() {
    }

    @Test
    void receiveDieDiceHasBeenRolled() {
    }

    @Test
    void receiveGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn() {
    }

    @Test
    void receiveGameStatePlayerWentBankrupt() {
    }

    @Test
    void receiveGameStateSetPlayerTurn() {
    }

    @Test
    void receiveMoneyGetMoneyFromBank() {
    }

    @Test
    void receiveMoneyPayMoneyToBank() {
    }

    @Test
    void receiveMoneyTransferMoneyBetweenPlayers() {
    }

    @Test
    void receiveMovingMovePlayerToTile() {
    }

    @Test
    void receivePropertyOfferToBuy() {
    }

    @Test
    void receivePropertyRemovePropertyOwnership() {
    }

    @Test
    void receivePropertySetPropertyMortgagedStatus() {
    }

    @Test
    void receivePropertySetPropertyOwnership() {
    }

    @Test
    void receivePropertyUpDowngradeProperty() {
    }

    @Test
    void getGame() {
        assertNull(gameNetworkingClientLogic.getGame());
    }

    @Test
    void getDtoGameDTOHandler() {
        assertNull(gameNetworkingClientLogic.getDtoGameDTOHandler());
    }

    @Test
    void getObjectMapper() {
        assertEquals(objectMapper, gameNetworkingClientLogic.getObjectMapper());
    }

    @Test
    void getMpClient() {
        assertNull(gameNetworkingClientLogic.getMpClient());
    }

    @Test
    void setGame() {
        gameNetworkingClientLogic.setGame(null);
    }

    @Test
    void setDtoGameDTOHandler() {
        gameNetworkingClientLogic.setDtoGameDTOHandler(null);
    }

    @Test
    void setObjectMapper() {
        gameNetworkingClientLogic.setObjectMapper(null);
    }

    @Test
    void setMpClient() {
        gameNetworkingClientLogic.setMpClient(null);
    }
}