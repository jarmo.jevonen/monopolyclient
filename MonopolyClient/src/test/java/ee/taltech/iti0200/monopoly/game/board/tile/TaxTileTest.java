package ee.taltech.iti0200.monopoly.game.board.tile;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaxTileTest {
    private TaxTile taxTile;

    @BeforeEach
    void setUp() {
        taxTile = new TaxTile();

        taxTile.setTileName("FooBar");
        taxTile.setPosition(3);
        taxTile.setOwnable(true);

        taxTile.setTaxAmount(10);
    }

    @Test
    void testToString() {
        assertEquals("3) FooBar", taxTile.toString());
    }

    @Test
    void getTaxAmount() {
        assertEquals(10, taxTile.getTaxAmount());
    }

    @Test
    void setTaxAmount() {
        taxTile.setTaxAmount(20);
        assertEquals(20, taxTile.getTaxAmount());
    }
}