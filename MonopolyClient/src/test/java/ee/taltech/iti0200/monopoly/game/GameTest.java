package ee.taltech.iti0200.monopoly.game;

import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.UtilityPropertyTile;
import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
    private Game game;
    private List<Player> players;

    @BeforeEach
    void setUp() {
        game = new Game(null, null, null, null, null, null);

        Player player1 = new Player();
        player1.setName("Alice");
        player1.setConnectionId(1234);
        player1.setPlayerId("abcde");

        Player player2 = new Player();
        player2.setName("Bob");
        player2.setConnectionId(1234);
        player2.setPlayerId("qwerty");
        players = new ArrayList<>(List.of(player1, player2));
        game.setPlayers(players);
    }


    @Test
    void addAIPlayer() {
        assertThrows(NullPointerException.class, () -> game.addAIPlayer());
    }

    @Test
    void getPropertyTileBuildingsCountOrMortgageString() {
        RegularPropertyTile regularPropertyTile8 = new RegularPropertyTile();
        regularPropertyTile8.setPosition(8);
        RegularPropertyTile regularPropertyTile9 = new RegularPropertyTile();
        regularPropertyTile9.setPosition(9);
        game.setTiles(new ArrayList<>(List.of(regularPropertyTile8, regularPropertyTile9)));
        game.getPropertyTileBuildingsCountOrMortgageString(0);
    }

    @Test
    void getPropertyTileBuildingsCountOrMortgageStringUtility() {
        UtilityPropertyTile utilityPropertyTile = new UtilityPropertyTile();
        utilityPropertyTile.setPosition(8);
        game.setTiles(new ArrayList<>(List.of(utilityPropertyTile)));
        game.getPropertyTileBuildingsCountOrMortgageString(0);
    }

    @Test
    void playerReadyStatusChanged() {
    }

    @Test
    void chooseSwitchCardOrPay() {
    }

    @Test
    void useGetOutOfJailCard() {
    }

    @Test
    void rollTheDiceButtonOnClick() {
    }

    @Test
    void endTurn() {
    }

    @Test
    void payToGetOutOfJail() {
    }

    @Test
    void setPropertyPurchaseDecision() {
    }

    @Test
    void setPropertyPropertyUpDowngradeDecision() {
    }

    @Test
    void receivedPlayerReadyStatusUpdate() {
    }

    @Test
    void receiveCardOfferSwitchACardOrPay() {
    }

    @Test
    void receiveCardSetPlayerOwnedGetOutOfJailCardsCount() {
    }

    @Test
    void receiveCardTakeACard() {
    }

    @Test
    void receiveDieDiceHasBeenRolled() {
    }

    @Test
    void receiveGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn() {
    }

    @Test
    void receiveGameStatePlayerWentBankrupt() {
    }

    @Test
    void receiveGameStateSetPlayerTurn() {
    }

    @Test
    void receiveMoneyGetMoneyFromBank() {
    }

    @Test
    void receiveMoneyPayMoneyToBank() {
    }

    @Test
    void receiveMoneyTransferMoneyBetweenPlayers() {
    }

    @Test
    void receiveMovingMovePlayerToTile() {
    }

    @Test
    void receivePropertyOfferToBuy() {
    }

    @Test
    void receivePropertyRemovePropertyOwnership() {
    }

    @Test
    void receivePropertySetPropertyMortgagedStatus() {
    }

    @Test
    void receivePropertySetPropertyOwnership() {
    }

    @Test
    void receivePropertySetPropertyUpDowngradeProperty() {
    }

    @Test
    void end() {
        assertThrows(NullPointerException.class, () -> game.end());
    }

    @Test
    void getWindowHeight() {
        assertThrows(NullPointerException.class, () -> game.getWindowHeight());
    }

    @Test
    void getWindowWidth() {
        assertThrows(NullPointerException.class, () -> game.getWindowWidth());
    }

    @Test
    void getPlayers() {
        game.getPlayers();
    }

    @Test
    void getTiles() {
        game.getTiles();
    }

    @Test
    void getCardsChance() {
        game.getCardsChance();
    }

    @Test
    void getCardsCommunityChest() {
        game.getCardsCommunityChest();
    }

    @Test
    void getGameNetworkingClientLogic() {
        game.getGameNetworkingClientLogic();
    }

    @Test
    void setPlayers() {
        game.setPlayers(new ArrayList<>());
    }

    @Test
    void setTiles() {
        game.setTiles(new ArrayList<>());
    }

    @Test
    void setCardsChance() {
        game.setCardsChance(new ArrayList<>());
    }

    @Test
    void setCardsCommunityChest() {
        game.setCardsCommunityChest(new ArrayList<>());
    }

    @Test
    void setCurrentlyOpenedCloseUpPropertyTile() {
        game.setCurrentlyOpenedCloseUpPropertyTile(0);
    }
}