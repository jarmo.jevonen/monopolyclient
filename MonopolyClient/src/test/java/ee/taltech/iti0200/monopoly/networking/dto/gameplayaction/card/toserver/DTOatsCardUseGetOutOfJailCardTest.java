package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsCardUseGetOutOfJailCardTest {
    private DTOatsCardUseGetOutOfJailCard dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsCardUseGetOutOfJailCard();
        dto.setPlayerId("FooId");
        dto.setUseCard(true);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void isUseCard() {
        assertTrue(dto.isUseCard());
    }
}