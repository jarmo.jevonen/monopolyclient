package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsPropertyMortgageDecisionTest {
    private DTOatsPropertyMortgageDecision dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsPropertyMortgageDecision();
        dto.setMortgaged(true);
        dto.setPropertyIndex(8);
        dto.setPlayerId("abcde");
    }

    @Test
    void getPlayerId() {
        assertEquals("abcde", dto.getPlayerId());
    }

    @Test
    void getPropertyIndex() {
        assertEquals(8, dto.getPropertyIndex());
    }

    @Test
    void isMortgaged() {
        assertTrue(dto.isMortgaged());
    }
}