package ee.taltech.iti0200.monopoly.game.card;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {
    private Deck deck;
    private List<Card> chanceCards;
    private List<Card> communityChestCards;
    private Card chanceCard1;
    private Card chanceCard2;
    private Card communityChestCard1;
    private Card communityChestCard2;

    @BeforeEach
    void setUp() {
        deck = new Deck();
        chanceCard1 = new Card();
        chanceCard1.setType(CardType.CHANCE);
        chanceCard2 = new Card();
        chanceCard2.setType(CardType.CHANCE);
        communityChestCard1 = new Card();
        communityChestCard1.setType(CardType.COMMUNITY_CHEST);
        communityChestCard2 = new Card();
        communityChestCard2.setType(CardType.COMMUNITY_CHEST);
        chanceCards = new ArrayList<>(List.of(chanceCard1));
        communityChestCards = new ArrayList<>(List.of(communityChestCard1));

        deck.setChanceCards(chanceCards);
        deck.setCommunityChestCards(communityChestCards);
    }

    @Test
    void getChanceCards() {
        assertEquals(chanceCards, deck.getChanceCards());
    }

    @Test
    void getCommunityChestCards() {
        assertEquals(communityChestCards, deck.getCommunityChestCards());
    }

    @Test
    void setChanceCards() {
        deck.setChanceCards(new ArrayList<>(List.of(chanceCard2)));
        assertEquals(List.of(chanceCard2), deck.getChanceCards());
    }

    @Test
    void setCommunityChestCards() {
        deck.setCommunityChestCards(new ArrayList<>(List.of(communityChestCard2)));
        assertEquals(List.of(communityChestCard2), deck.getCommunityChestCards());
    }
}