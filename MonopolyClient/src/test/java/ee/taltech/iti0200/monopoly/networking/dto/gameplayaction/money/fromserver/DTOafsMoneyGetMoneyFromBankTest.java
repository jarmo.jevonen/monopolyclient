package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsMoneyGetMoneyFromBankTest {
    private DTOafsMoneyGetMoneyFromBank dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsMoneyGetMoneyFromBank();
        dto.setPlayerId("FooId");
        dto.setPlayerNewBalance(100);
        dto.setReceivedAmount(30);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void getReceivedAmount() {
        assertEquals(30, dto.getReceivedAmount());
    }

    @Test
    void getPlayerNewBalance() {
        assertEquals(100, dto.getPlayerNewBalance());
    }
}