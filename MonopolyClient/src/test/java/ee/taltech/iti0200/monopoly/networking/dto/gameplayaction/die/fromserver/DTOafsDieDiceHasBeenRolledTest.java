package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.die.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsDieDiceHasBeenRolledTest {
    private DTOafsDieDiceHasBeenRolled dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsDieDiceHasBeenRolled();
        dto.setDie1Dots(3);
        dto.setDie2Dots(4);
    }

    @Test
    void getDie1Dots() {
        assertEquals(3, dto.getDie1Dots());
    }

    @Test
    void getDie2Dots() {
        assertEquals(4, dto.getDie2Dots());
    }
}