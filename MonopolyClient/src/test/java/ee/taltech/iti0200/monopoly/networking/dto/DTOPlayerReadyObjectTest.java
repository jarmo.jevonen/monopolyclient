package ee.taltech.iti0200.monopoly.networking.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOPlayerReadyObjectTest {
    private DTOPlayerReadyObject dto;

    @BeforeEach
    void setUp() {
        dto = new DTOPlayerReadyObject();
        dto.setReadyStatus(false);
        dto.setPlayerName("Foo");
        dto.setPlayerId("abcde");
    }

    @Test
    void getPlayerName() {
        assertEquals("Foo", dto.getPlayerName());
    }

    @Test
    void getPlayerId() {
        assertEquals("abcde", dto.getPlayerId());
    }

    @Test
    void isReadyStatus() {
        assertFalse(dto.isReadyStatus());
    }

    @Test
    void newInstance() {
        dto = new DTOPlayerReadyObject("Foo", "abcd", true);
        assertEquals("Foo", dto.getPlayerName());
    }
}