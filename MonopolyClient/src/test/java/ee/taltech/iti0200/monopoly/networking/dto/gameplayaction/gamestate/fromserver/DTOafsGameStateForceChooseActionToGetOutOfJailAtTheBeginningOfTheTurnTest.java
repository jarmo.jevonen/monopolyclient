package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurnTest {
    private DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn();
        dto.setPlayerId("FooId");
        dto.setAllowRollingTheDice(true);
        dto.setAllowPaying(true);
        dto.setAllowUsingACard(true);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void isAllowRollingTheDice() {
        assertTrue(dto.isAllowRollingTheDice());
    }

    @Test
    void isAllowPaying() {
        assertTrue(dto.isAllowPaying());
    }

    @Test
    void isAllowUsingACard() {
        assertTrue(dto.isAllowUsingACard());
    }
}