package ee.taltech.iti0200.monopoly.game.player;

import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    private Player player;
    private RegularPropertyTile regularPropertyTile;
    private Card card;

    @BeforeEach
    void setUp() {
        player = new Player();

        regularPropertyTile = new RegularPropertyTile();
        card = new Card();

        player.setPlayerId("FooId");
        player.setBalance(200);
        player.setOwnedPropertyTiles(List.of(regularPropertyTile));
        player.setOwnedCards(List.of(card));
        player.setSequentialDoublesCount(2);
        player.setPosition(30);
        player.setName("FooName");
        player.setInJail(false);
        player.setConnectionId(5);
        player.setAI(true);
        player.setHasGoneBankrupt(false);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", player.getPlayerId());
    }

    @Test
    void getBalance() {
        assertEquals(200, player.getBalance());
    }

    @Test
    void getOwnedPropertyTiles() {
        assertEquals(List.of(regularPropertyTile), player.getOwnedPropertyTiles());
    }

    @Test
    void getOwnedCards() {
        assertEquals(List.of(card), player.getOwnedCards());
    }

    @Test
    void getSequentialDoublesCount() {
        assertEquals(2, player.getSequentialDoublesCount());
    }

    @Test
    void getPosition() {
        assertEquals(30, player.getPosition());
    }

    @Test
    void getName() {
        assertEquals("FooName", player.getName());
    }

    @Test
    void isInJail() {
        assertFalse(player.isInJail());
    }

    @Test
    void isAI() {
        assertTrue(player.isAI);
    }

    @Test
    void isBankrupt() {
        assertFalse(player.hasGoneBankrupt);
    }

    @Test
    void getConnectionId() {
        assertEquals(5, player.getConnectionId());
    }

    @Test
    void setPlayerId() {
        player.setPlayerId("BarId");
        assertEquals("BarId", player.getPlayerId());
    }

    @Test
    void setBalance() {
        player.setBalance(100);
        assertEquals(100, player.getBalance());
    }

    @Test
    void setOwnedPropertyTiles() {
        player.setOwnedPropertyTiles(new ArrayList<>());
        assertEquals(new ArrayList<>(), player.getOwnedPropertyTiles());
    }

    @Test
    void setOwnedCards() {
        player.setOwnedCards(new ArrayList<>());
        assertEquals(new ArrayList<>(), player.getOwnedCards());
    }

    @Test
    void setSequentialDoublesCount() {
        player.setSequentialDoublesCount(3);
        assertEquals(3, player.getSequentialDoublesCount());
    }

    @Test
    void setPosition() {
        player.setPosition(10);
        assertEquals(10, player.getPosition());
    }

    @Test
    void setName() {
        player.setName("BarName");
        assertEquals("BarName", player.getName());
    }

    @Test
    void setInJail() {
        player.setInJail(true);
        assertTrue(player.isInJail());
    }

    @Test
    void setConnectionId() {
        player.setConnectionId(8);
        assertEquals(8, player.getConnectionId());
    }

}