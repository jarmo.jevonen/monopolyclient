package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.moving.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsMovingMovePlayerToTile {
    private String playerId;
    private int tileIndex;
    private boolean toJail;
    private boolean moveBackwards;
}
