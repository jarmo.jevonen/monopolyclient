package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.die.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsDieDiceHasBeenRolled {
    private int die1Dots;
    private int die2Dots;
}
