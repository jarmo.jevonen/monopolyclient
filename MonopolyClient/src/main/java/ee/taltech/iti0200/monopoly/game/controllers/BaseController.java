package ee.taltech.iti0200.monopoly.game.controllers;

import ee.taltech.iti0200.monopoly.game.Game;

public abstract class BaseController {
    protected Game game;

    public void setApplication(Game game) {
        this.game = game;
    }
}
