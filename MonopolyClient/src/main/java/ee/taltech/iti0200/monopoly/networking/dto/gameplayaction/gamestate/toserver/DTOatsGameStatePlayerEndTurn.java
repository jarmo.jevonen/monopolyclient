package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsGameStatePlayerEndTurn {
    private String PlayerId;
}
