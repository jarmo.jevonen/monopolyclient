package ee.taltech.iti0200.monopoly.game.controllers;

import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import javafx.scene.control.Button;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BoardButton extends Button {
    private Tile tile;
}
