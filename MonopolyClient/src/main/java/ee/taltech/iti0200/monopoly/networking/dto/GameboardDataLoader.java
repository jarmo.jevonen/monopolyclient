package ee.taltech.iti0200.monopoly.networking.dto;

import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.player.Player;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GameboardDataLoader {

    public static List<Player> loadPlayersFromJsonString(String playersJsonString) {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Player> players = null;
        try {
            players = objectMapper.readValue(playersJsonString, new TypeReference<List<Player>>(){});
        } catch (IOException e) {
            throw new RuntimeException("There was an error trying to map players' json string into player objects: " + e);
        }

        return players;

    }

    public static List<Tile> loadTilesFromJsonString(String tilesString) {
        String[] lines = tilesString.split("\\r?\\n");

        List<Tile> tiles = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (String line : lines) {
            try {
                tiles.add(objectMapper.readValue(line, Tile.class));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return tiles;

    }

    public static List<Card> loadCardsFromFiles(String cardsString) {
        String[] lines = cardsString.split("\\r?\\n");

        List<Card> cards = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (String line : lines) {
            try {
                cards.add(objectMapper.readValue(line, Card.class));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return cards;
    }
}
