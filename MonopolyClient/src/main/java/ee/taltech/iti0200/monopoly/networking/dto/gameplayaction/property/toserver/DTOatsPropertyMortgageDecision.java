package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsPropertyMortgageDecision {
    private String playerId;
    private int propertyIndex;
    // true for set mortgage, false for unmortgage
    private boolean mortgaged;
}
