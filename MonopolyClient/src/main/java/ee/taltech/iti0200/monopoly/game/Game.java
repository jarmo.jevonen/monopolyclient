package ee.taltech.iti0200.monopoly.game;

import ee.taltech.iti0200.monopoly.Main;
import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardAction;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.controllers.GameController;
import ee.taltech.iti0200.monopoly.game.controllers.MenuController;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.networking.client.GameNetworkingClientLogic;
import ee.taltech.iti0200.monopoly.networking.dto.DTOPlayerReadyObject;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Game {

    private Main main;
    @Getter
    private GameController gameController;
    private MenuController menuController;
    private Stage primaryStage;
    private Scene menuScene;
    private Scene gameScene;

    @Getter
    @Setter
    private List<Player> players;

    @Getter
    @Setter
    private List<Tile> tiles;

    @Getter
    @Setter
    private List<Card> cardsChance = new ArrayList<>();
    @Getter
    @Setter
    private List<Card> cardsCommunityChest = new ArrayList<>();

    @Getter
    private GameNetworkingClientLogic gameNetworkingClientLogic;

    @Setter
    private int currentlyOpenedCloseUpPropertyTile;

    public Game(Stage primaryStage, Scene menuScene, Scene gameScene, MenuController menuController,
                GameController gameController, Main main) {
        this.main = main;
        this.primaryStage = primaryStage;
        this.menuScene = menuScene;
        this.gameScene = gameScene;
        this.menuController = menuController;
        this.gameController = gameController;

        if (primaryStage != null) primaryStage.setScene(menuScene);
    }

    public void startTheGame() {
        Platform.runLater(() -> {
            gameController.setUpGameBoard(players);
            primaryStage.setScene(gameScene);
            Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
            if (primaryScreenBounds.getMaxY() <= 1080 && primaryScreenBounds.getMaxX() <= 1920) primaryStage.setFullScreen(true);
        });

    }

    // When client disconnects, reinitialize application.
    public void clientDisconnected() {
        Platform.runLater(() -> {
            try {
                main.initializeApplication();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    // Gets called by the MenuController, sets up the KryoNet client, enables the checkbox to toggle player ready-status.
    public void connectToServer(String ipAddress, int port, String userName, String lobbyId, boolean joinLobby) {
        this.gameNetworkingClientLogic = new GameNetworkingClientLogic(this);
        gameNetworkingClientLogic.connectToServer(ipAddress, port, userName, lobbyId, joinLobby);
    }

    // Gets called by the MenuController, when client clicks on add-AI-player button.
    public void addAIPlayer() {
        gameNetworkingClientLogic.sendAddAIPlayer();
    }

    public String getPropertyTileBuildingsCountOrMortgageString(int propertyTileIndex) {
        if (tiles.get(propertyTileIndex) instanceof RegularPropertyTile) {
            RegularPropertyTile regularPropertyTile = (RegularPropertyTile) tiles.get(propertyTileIndex);
            if (regularPropertyTile.isMortgaged()) return "P";
            return String.valueOf(regularPropertyTile.getBuildingsCount());
        } else {
            PropertyTile propertyTile = (PropertyTile) tiles.get(propertyTileIndex);
            if (propertyTile.isMortgaged()) return "P";
            return String.valueOf(0);
        }
    }


    // ================== Actions FROM user interface ==================

    // Gets called by the MenuController, announces player's new ready-status.
    public void playerReadyStatusChanged(boolean readyStatus) {
        gameNetworkingClientLogic.sendReadyStatus(readyStatus);
    }


    // Call when user is presented with a card such as 'Take a chance-type card or pay 10$' and user makes a choice. Boolean 'pay' is 'true' when player chooses to pay, 'false' when player chooses to take another type of card.
    public void chooseSwitchCardOrPay(boolean pay) {
        gameNetworkingClientLogic.sendCardChoice(pay);
        // Probably also have to close the pop up here.
    }

    // Call when the user is presented with a choice on how to get out of jail and chooses to use their 'get out of jail for free' card.
    public void useGetOutOfJailCard() {
        gameNetworkingClientLogic.sendCardUseGetOutOfJailCard(true);
    }

    // Gets called by the GameController, forwards the client's dice button click to GameNetworkingClientLogic for sending a dto to the server.
    public void rollTheDiceButtonOnClick() {
        gameNetworkingClientLogic.sendDieRollTheDice();
    }

    // Call when the user chooses to end their turn.
    public void goBankrupt() {
        gameNetworkingClientLogic.sendGameStatePlayerBankruptcyAcknowledgement();
    }

    // Call when the user chooses to end their turn.
    public void endTurn() {
        gameNetworkingClientLogic.sendGameStatePlayerEndTurn();
    }

    // Call when the user is presented with a choice on how to get out of jail and chooses to pay.
    public void payToGetOutOfJail() {
        gameNetworkingClientLogic.sendMoneyPayToGetOutOfJail();
    }

    // Call when the user decides whether to purchase a property.
    public void setPropertyPurchaseDecision( boolean confirmPurchase) {
        gameNetworkingClientLogic.sendPropertyPurchaseDecision(confirmPurchase);
    }

    // Call when the user wants to change property's mortgaged status.
    public void setPropertyPropertyUpDowngradeDecision(boolean upgrade) { // these parameters can be swapped out for whatever works better coming from the controller or smth.
        // boolean upgrade - true for upgrade, false for downgrade
        gameNetworkingClientLogic.sendPropertyUpDowngradeDecision(currentlyOpenedCloseUpPropertyTile, upgrade);
    }



    // ================== Actions TO User interface ==================

    // Gets called by the GameNetworkClientLogic, sends the list to the MenuController for UI updating.
    public void receivedPlayerReadyStatusUpdate(List<DTOPlayerReadyObject> playerReadyObjects) {
        menuController.refreshPlayersStatus(playerReadyObjects);
        menuController.setDisabledPlayerReadyCheckBox(!gameNetworkingClientLogic.getMpClient().getClient().isConnected() || playerReadyObjects.size() <= 1);
    }

    public void receiveCardOfferSwitchACardOrPay(Player player, CardType currentCardType, CardType anotherCardType, double amountToPayIfNotSwitching) {
        // Not really needed as 'receiveCardTakeACard' already handles this case.
    }

    public void receiveCardSetPlayerOwnedGetOutOfJailCardsCount(Player player, int getOutOfJailCardsCount) {}

    public void receiveCardTakeACard(Player player, Card card) {
        String cardTitle = card.getType().equals(CardType.CHANCE) ? "LOOS" : "ÜHISFOND";
        boolean showSwitchOrPayButtons = (player.getPlayerId().equals(gameNetworkingClientLogic.getMpClient().getPlayerId()) && card.getAction().equals(CardAction.PAY_OR_SWITCH));
        gameController.showTakeACard(cardTitle, card.getText1(), card.getText2(), card.getText3(), card.getText4(), showSwitchOrPayButtons, !showSwitchOrPayButtons, showSwitchOrPayButtons, player.getName());
    }

    // Gets called by the GameNetworkClientLogic, sends the dice numbers to the MenuController for UI updating.
    public void receiveDieDiceHasBeenRolled(int die1Dots, int die2Dots) {
        gameController.showDiceRoll(die1Dots, die2Dots);
    }

    public void receiveGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(Player player, boolean allowRollingTheDice, boolean allowPaying, boolean allowUsingACard) {
        gameController.givePlayerGetOutOfJailOptions(player, allowRollingTheDice, allowPaying, allowUsingACard);
    }

    public void receiveGameStatePlayerWentBankrupt(Player player) {
        player.setHasGoneBankrupt(true);
        for (PropertyTile propertyTile : player.getOwnedPropertyTiles()) {
            propertyTile.setMortgaged(false);
            gameController.propertySetMortgagedStatus(propertyTile.getPosition(), player.getPlayerId(), false);
            if (propertyTile instanceof RegularPropertyTile) ((RegularPropertyTile) propertyTile).setBuildingsCount(0);
            gameController.updateBuildingsCountOnTile(propertyTile.getPosition(), 0);
            gameController.propertySetOwnership("", propertyTile.getPosition());
            propertyTile.setOwner(null);
        }
        gameController.disablePlayerPieceAndRectangle(player.getPlayerId(), players.indexOf(player));
    }

    public void receiveGameStateSetPlayerTurn(Player player) {
        gameController.showPlayerTurn(player.getPlayerId(), player.getPlayerId().equals(gameNetworkingClientLogic.getMpClient().getPlayerId()));
    }

    public void receiveGameStateWarnBankruptcy() {
        gameController.showBankruptcyWarning();
    }

    public void receiveGameStateWinner(Player player) {
        gameController.showWinnerScreen(player.getName());
    }

    public void receiveMoneyGetMoneyFromBank(Player player, double playerNewBalance, double receivedAmount) {
        player.setBalance(playerNewBalance);
        gameController.showChangePlayerBalance("", 0, player.getPlayerId(), playerNewBalance, receivedAmount, true, false);
    }

    public void receiveMoneyPayMoneyToBank(Player player, double playerNewBalance, double paidAmount) {
        player.setBalance(playerNewBalance);
        gameController.showChangePlayerBalance(player.getPlayerId(), playerNewBalance, "", 0, paidAmount,false, true);
    }

    public void receiveMoneyTransferMoneyBetweenPlayers(Player payerPlayer, double payerPlayerNewBalance, Player payeePlayer, double payeePlayerNewBalance, double transferredAmount) {
        payerPlayer.setBalance(payerPlayerNewBalance);
        payeePlayer.setBalance(payeePlayerNewBalance);
        gameController.showChangePlayerBalance(payerPlayer.getPlayerId(), payerPlayerNewBalance, payeePlayer.getPlayerId(), payeePlayerNewBalance, transferredAmount,false, false);
    }

    public void receiveMovingMovePlayerToTile(Player player, Tile tile, boolean moveBackwards, boolean toJail) {
        try {
            gameController.getGameControlsHBox().setDisable(true);
            TimeUnit.MILLISECONDS.sleep(1500);
            gameController.getGameControlsHBox().setDisable(false);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        player.setPosition(tile.getPosition());
        gameController.movePlayer(player.getPlayerId(), tile.getPosition(), moveBackwards, toJail);
    }

    public void receivePropertyOfferToBuy(Player player, PropertyTile propertyTile) {
        gameController.propertyOfferToBuy(propertyTile);
    }

    public void receivePropertyRemovePropertyOwnership(Player player, List<PropertyTile> tilesFromWhichToRemovePropertyOwnership) {
        tilesFromWhichToRemovePropertyOwnership.forEach(propertyTile -> {
            propertyTile.setOwner(null);
            gameController.propertySetOwnership("-", propertyTile.getPosition());
        });
        player.getOwnedPropertyTiles().clear();
    }

    public void receivePropertySetPropertyMortgagedStatus(PropertyTile propertyTile, boolean mortgagedStatus) {
        propertyTile.setMortgaged(mortgagedStatus);
        gameController.propertySetMortgagedStatus(propertyTile.getPosition(), propertyTile.getOwner().getPlayerId(), mortgagedStatus);

        String closeUpInteractionsLabelText = "";
        if (mortgagedStatus) {
            closeUpInteractionsLabelText = "M";
        } else {
            closeUpInteractionsLabelText = "0";
        }
        gameController.setPropertyCardCloseUpInteractionsBuildingCountLabel(closeUpInteractionsLabelText);
    }

    public void receivePropertySetPropertyOwnership(Player player, PropertyTile propertyTile) {
        propertyTile.setOwner(player);
        player.getOwnedPropertyTiles().add(propertyTile);
        gameController.propertySetOwnership(player.getPlayerId(), propertyTile.getPosition());
    }

    public void receivePropertySetPropertyUpDowngradeProperty(RegularPropertyTile regularPropertyTile, int upDowngradeToLevel) {
        regularPropertyTile.setBuildingsCount(upDowngradeToLevel);
        gameController.setPropertyCardCloseUpInteractionsBuildingCountLabel(upDowngradeToLevel == 5 ? "H" : String.valueOf(upDowngradeToLevel));
        gameController.updateBuildingsCountOnTile(regularPropertyTile.getPosition(), upDowngradeToLevel);
    }




    public void end() {
        primaryStage.setScene(menuScene);
    }

    public double getWindowHeight() {
        return primaryStage.getHeight();
    }

    public double getWindowWidth() {
        return primaryStage.getWidth();
    }

    public void receiveLobbyId(String lobbyId) {
        menuController.showLobbyId(lobbyId);
    }
}