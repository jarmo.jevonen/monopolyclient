package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsMoneyTransferMoneyBetweenPlayers {
    private String payerPlayerId;
    private String payeePlayerId;
    private double transferredAmount;
    private double payerPlayerNewBalance;
    private double payeePlayerNewBalance;
}
