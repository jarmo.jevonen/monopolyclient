package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsCardChoice {
    private String playerId;
    // true for pay, false for switch
    private boolean pay;
}
