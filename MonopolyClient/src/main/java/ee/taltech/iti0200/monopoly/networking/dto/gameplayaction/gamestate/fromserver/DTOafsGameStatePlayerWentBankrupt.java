package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsGameStatePlayerWentBankrupt {
    private String PlayerId;
}
