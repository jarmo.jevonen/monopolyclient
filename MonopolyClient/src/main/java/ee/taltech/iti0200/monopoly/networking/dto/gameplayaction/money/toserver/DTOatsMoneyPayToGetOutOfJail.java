package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsMoneyPayToGetOutOfJail {
    private String playerId;
}
