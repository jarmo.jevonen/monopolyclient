package ee.taltech.iti0200.monopoly;

import ee.taltech.iti0200.monopoly.game.Game;
import ee.taltech.iti0200.monopoly.game.controllers.GameController;
import ee.taltech.iti0200.monopoly.game.controllers.MenuController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;


public class Main extends Application {
    private Stage primaryStage;
    private FXMLLoader menuLoader;
    private FXMLLoader gameLoader;
    private MenuController menuController;
    private GameController gameController;
    private Scene menuScene;
    private Scene gameScene;
    private Game game;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        initializeApplication();

        primaryStage.setTitle("TalTech Monopoly");

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        System.out.println(primaryScreenBounds.getMaxY() + "x" + primaryScreenBounds.getMaxX());
        if (primaryScreenBounds.getMaxY() <= 1080 && primaryScreenBounds.getMaxX() <= 1920) primaryStage.setFullScreen(true);
        /*
        primaryStage.setMaxHeight(1180);
        primaryStage.setMaxWidth(1920);
        primaryStage.setFullScreen(true);*/

        /*primaryStage.setMaximized(false);
        primaryStage.setWidth(1920);
        primaryStage.setHeight(1180);*/

        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });

        primaryStage.show();
    }

    public void initializeApplication() throws Exception {

        menuLoader = new FXMLLoader(getClass().getResource("game/menu.fxml"));
        gameLoader = new FXMLLoader(getClass().getResource("game/game.fxml"));

        menuController = new MenuController();
        gameController = new GameController();
        menuLoader.setController(menuController);
        gameLoader.setController(gameController);

        menuScene = new Scene(menuLoader.load());
        gameScene = new Scene(gameLoader.load());
        game = new Game(primaryStage, menuScene, gameScene, menuController, gameController, this);
        menuController.setApplication(game);
        gameController.setApplication(game);
    }

    public static void main(String[] args) {
        launch(args);
    }

}