package ee.taltech.iti0200.monopoly.networking.dto;

import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.networking.client.GameNetworkingClientLogic;
import ee.taltech.iti0200.monopoly.networking.client.MPClient;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver.DTOafsCardOfferSwitchACardOrPay;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver.DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver.DTOafsCardTakeACard;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.die.fromserver.DTOafsDieDiceHasBeenRolled;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.fromserver.*;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver.DTOafsMoneyGetMoneyFromBank;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver.DTOafsMoneyPayMoneyToBank;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver.DTOafsMoneyTransferMoneyBetweenPlayers;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.moving.fromserver.DTOafsMovingMovePlayerToTile;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.fromserver.*;
import ee.taltech.iti0200.monopoly.networking.packets.Packets;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

public class DTOGameDTOHandler {
    private GameNetworkingClientLogic gameNetworkingClientLogic;
    private static final String CLIENT_GAME_DTO_HANDLER_LOGGER_PREFIX = "CgDTO-Handler-> ";
    private ObjectMapper objectMapper;

    public DTOGameDTOHandler(GameNetworkingClientLogic gameNetworkingClientLogic) {
        this.gameNetworkingClientLogic = gameNetworkingClientLogic;
        this.objectMapper = new ObjectMapper();
    }


    // ===================== SEND: GameNetworkClientLogic --> DTOGameDTOHandler --> Server ====================

    // Called by GameNetworkClientLogic to send gameplay actions to the server.
    public void sendDTOGameplayPacket(String typeOfDTOGameplayAction, String dTOGameplayActionJsonString) {
        MPClient.getClientLogger().log(Level.INFO, CLIENT_GAME_DTO_HANDLER_LOGGER_PREFIX + ", DTO type: " + typeOfDTOGameplayAction + ", ActionString: " + dTOGameplayActionJsonString);
        Packets.PacketServerReceiveDTOGameplayAction packet = new Packets.PacketServerReceiveDTOGameplayAction();
        packet.setTypeOfDTOGameplayAction(typeOfDTOGameplayAction);
        packet.setDTOGameplayActionJsonString(dTOGameplayActionJsonString);
        packet.setLobbyId(gameNetworkingClientLogic.getLobbyId());
        gameNetworkingClientLogic.getMpClient().getClient().sendTCP(packet);
    }

    // Called by GameNetworkClientLogic to send player-ready-object dto-s to the server.
    public void sendDTOPlayerReadyStatus(DTOPlayerReadyObject dto) {
        MPClient.getClientLogger().log(Level.INFO, CLIENT_GAME_DTO_HANDLER_LOGGER_PREFIX + ", DTO type: " + DTOPlayerReadyObject.class.getSimpleName() + ", Status: " + dto.isReadyStatus());
        Packets.PacketServerReceivePlayerReadyObject packet = new Packets.PacketServerReceivePlayerReadyObject();
        try {
            packet.setPlayerReadyObjectDTOString(objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
        packet.setLobbyId(gameNetworkingClientLogic.getLobbyId());
        gameNetworkingClientLogic.getMpClient().getClient().sendTCP(packet);
    }


    // ===================== RECEIVE: ClientNetworkListener --> DTOGameDTOHandler ---> GameNetworkClientLogic =====================

    // Called by ClientNetworkListener when client receives player-ready-objects broadcast.
    public void receiveDTOPlayerReadyStatusesJsonString(String playerReadyStatusesJsonString) {
        MPClient.getClientLogger().log(Level.CONFIG, CLIENT_GAME_DTO_HANDLER_LOGGER_PREFIX + "Received: " + DTOPlayerReadyObject.class.getSimpleName() + "s, with JSON: " + playerReadyStatusesJsonString);
        try {
            List<DTOPlayerReadyObject> dtoPlayerReadyObjects = objectMapper.readValue(playerReadyStatusesJsonString, new TypeReference<List<DTOPlayerReadyObject>>(){});
            gameNetworkingClientLogic.receivePlayerReadyStatuses(dtoPlayerReadyObjects);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Called by ClientNetworkListener when client receives game data at the beginning of a game (tiles, cards).
    public void receiveDTOGameDataJsonStrings(String typeOfDTOGameboardData, String DTOGameboardDataJsonString) throws IOException {
        MPClient.getClientLogger().log(Level.CONFIG, CLIENT_GAME_DTO_HANDLER_LOGGER_PREFIX + "Received: " + typeOfDTOGameboardData + ", with JSON: " + DTOGameboardDataJsonString);

        // dto.gameboardData.players
        if (Player.class.getSimpleName().equals(typeOfDTOGameboardData)) {
            List<Player> players = GameboardDataLoader.loadPlayersFromJsonString(DTOGameboardDataJsonString);
            gameNetworkingClientLogic.receiveGameDataPlayers(players);
        }
        // dto.gameboardData.tiles
        if (Tile.class.getSimpleName().equals(typeOfDTOGameboardData)) {
            List<Tile> tiles = GameboardDataLoader.loadTilesFromJsonString(DTOGameboardDataJsonString);
            gameNetworkingClientLogic.receiveGameDataTiles(tiles);
        }
        // dto.gameboardData.cardsChance
        else if ((Card.class.getSimpleName() + "Chance").equals(typeOfDTOGameboardData)) {
            List<Card> cards = GameboardDataLoader.loadCardsFromFiles(DTOGameboardDataJsonString);
            gameNetworkingClientLogic.receiveGameDataCardsChance(cards);
        }
        // dto.gameboardData.cardsCommunityChest
        else if ((Card.class.getSimpleName() + "CommunityChest").equals(typeOfDTOGameboardData)) {
            List<Card> cards = GameboardDataLoader.loadCardsFromFiles(DTOGameboardDataJsonString);
            gameNetworkingClientLogic.receiveGameDataCardsCommunityChest(cards);
        }
    }

    // Called by ClientNetworkListener when client receives a gameplay action.
    public void receiveDTOGameplayJsonStrings(String typeOfDTOGameplayAction, String dTOGameplayActionJsonString) throws IOException {
        MPClient.getClientLogger().log(Level.INFO, CLIENT_GAME_DTO_HANDLER_LOGGER_PREFIX + "Received: " + typeOfDTOGameplayAction + ", with JSON: " + dTOGameplayActionJsonString);

        // dto.gameplayaction.card.fromserver
        if (DTOafsCardOfferSwitchACardOrPay.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsCardOfferSwitchACardOrPay dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsCardOfferSwitchACardOrPay.class);
            gameNetworkingClientLogic.receiveCardOfferSwitchACardOrPay(dto);
        }
        else if (DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount.class);
            gameNetworkingClientLogic.receiveCardSetPlayerOwnedGetOutOfJailCardsCount(dto);
        }
        else if (DTOafsCardTakeACard.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsCardTakeACard dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsCardTakeACard.class);
            gameNetworkingClientLogic.receiveCardTakeACard(dto);
        }

        // dto.gameplayaction.die.fromserver
        else if (DTOafsDieDiceHasBeenRolled.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsDieDiceHasBeenRolled dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsDieDiceHasBeenRolled.class);
            gameNetworkingClientLogic.receiveDieDiceHasBeenRolled(dto);
        }

        // dto.gameplayaction.gamestate.fromserver
        else if (DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn.class);
            gameNetworkingClientLogic.receiveGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(dto);
        }
        else if (DTOafsGameStatePlayerWentBankrupt.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsGameStatePlayerWentBankrupt dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsGameStatePlayerWentBankrupt.class);
            gameNetworkingClientLogic.receiveGameStatePlayerWentBankrupt(dto);
        }
        else if (DTOafsGameStateSetPlayerTurn.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsGameStateSetPlayerTurn dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsGameStateSetPlayerTurn.class);
            gameNetworkingClientLogic.receiveGameStateSetPlayerTurn(dto);
        }
        else if (DTOafsGameStateWarnBankruptcy.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsGameStateWarnBankruptcy dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsGameStateWarnBankruptcy.class);
            gameNetworkingClientLogic.receiveGameStateWarnBankruptcy(dto);
        }
        else if (DTOafsGameStateWinner.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsGameStateWinner dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsGameStateWinner.class);
            gameNetworkingClientLogic.receiveGameStateWinner(dto);
        }

        // dto.gameplayaction.money.fromserver
        else if (DTOafsMoneyGetMoneyFromBank.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsMoneyGetMoneyFromBank dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsMoneyGetMoneyFromBank.class);
            gameNetworkingClientLogic.receiveMoneyGetMoneyFromBank(dto);
        }
        else if (DTOafsMoneyPayMoneyToBank.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsMoneyPayMoneyToBank dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsMoneyPayMoneyToBank.class);
            gameNetworkingClientLogic.receiveMoneyPayMoneyToBank(dto);
        }
        else if (DTOafsMoneyTransferMoneyBetweenPlayers.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsMoneyTransferMoneyBetweenPlayers dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsMoneyTransferMoneyBetweenPlayers.class);
            gameNetworkingClientLogic.receiveMoneyTransferMoneyBetweenPlayers(dto);
        }

        // dto.gameplayaction.moving.fromserver
        else if (DTOafsMovingMovePlayerToTile.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsMovingMovePlayerToTile dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsMovingMovePlayerToTile.class);
            gameNetworkingClientLogic.receiveMovingMovePlayerToTile(dto);
        }

        // dto.gameplayaction.property.fromserver
        else if (DTOafsPropertyOfferToBuy.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsPropertyOfferToBuy dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsPropertyOfferToBuy.class);
            gameNetworkingClientLogic.receivePropertyOfferToBuy(dto);
        }
        else if (DTOafsPropertyRemovePropertyOwnership.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsPropertyRemovePropertyOwnership dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsPropertyRemovePropertyOwnership.class);
            gameNetworkingClientLogic.receivePropertyRemovePropertyOwnership(dto);
        }
        else if (DTOafsPropertySetPropertyMortgagedStatus.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsPropertySetPropertyMortgagedStatus dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsPropertySetPropertyMortgagedStatus.class);
            gameNetworkingClientLogic.receivePropertySetPropertyMortgagedStatus(dto);
        }
        else if (DTOafsPropertySetPropertyOwnership.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsPropertySetPropertyOwnership dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsPropertySetPropertyOwnership.class);
            gameNetworkingClientLogic.receivePropertySetPropertyOwnership(dto);
        }
        else if (DTOafsPropertyUpDowngradeProperty.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOafsPropertyUpDowngradeProperty dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOafsPropertyUpDowngradeProperty.class);
            gameNetworkingClientLogic.receivePropertyUpDowngradeProperty(dto);
        }
    }
}
