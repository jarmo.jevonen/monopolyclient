package ee.taltech.iti0200.monopoly.game.board.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RailroadPropertyTile extends PropertyTile{

    private PropertyTileGroup propertyTileGroup;

    private double rentWithOneStation;
    private double rentWithTwoStations;
    private double rentWithThreeStations;
    private double rentWithFourStations;

    @Override
    public String toString() {
        return String.format("%d) %s, %s", getPosition(), getTileName(), getPropertyColor());
    }
}
