package ee.taltech.iti0200.monopoly.networking.client;

import ee.taltech.iti0200.monopoly.game.Game;
import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.networking.dto.DTOGameDTOHandler;
import ee.taltech.iti0200.monopoly.networking.dto.DTOPlayerReadyObject;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver.DTOafsCardOfferSwitchACardOrPay;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver.DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver.DTOafsCardTakeACard;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.toserver.DTOatsCardChoice;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.toserver.DTOatsCardUseGetOutOfJailCard;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.die.fromserver.DTOafsDieDiceHasBeenRolled;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.die.toserver.DTOatsDieRollTheDice;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.fromserver.*;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerBankruptcyAcknowledgement;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerEndTurn;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver.DTOafsMoneyGetMoneyFromBank;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver.DTOafsMoneyPayMoneyToBank;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver.DTOafsMoneyTransferMoneyBetweenPlayers;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.toserver.DTOatsMoneyPayToGetOutOfJail;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.moving.fromserver.DTOafsMovingMovePlayerToTile;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.fromserver.*;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.toserver.DTOatsPropertyMortgageDecision;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.toserver.DTOatsPropertyPurchaseDecision;
import ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.toserver.DTOatsPropertyUpDowngradeDecision;
import ee.taltech.iti0200.monopoly.networking.packets.Packets;
import lombok.Data;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Data
public class GameNetworkingClientLogic {
    private Game game;
    private DTOGameDTOHandler dtoGameDTOHandler;
    private ObjectMapper objectMapper;
    private MPClient mpClient;
    private String lobbyId;

    public GameNetworkingClientLogic(Game game) {
        this.game = game;
        this.dtoGameDTOHandler = new DTOGameDTOHandler(this);
        this.objectMapper = new ObjectMapper();
    }

    // Called by Game, creates an instance of MPClient and creates a connection to the server.
    public void connectToServer(String ipAddress, int port, String userName, String lobbyId, boolean joinLobby) {
        this.lobbyId = lobbyId;
        mpClient = new MPClient(ipAddress, port, userName, this,  lobbyId, joinLobby);
    }

    // Called by Game, sends create AI player packet to server.
    public void sendAddAIPlayer() {
        Packets.PacketRegisterAIPlayerFromClient packet = new Packets.PacketRegisterAIPlayerFromClient();
        packet.setLobbyId(lobbyId);
        mpClient.getClient().sendTCP(packet);
    }



    // ==================  Game --> GameNetworkingClientLogic --> DTOGameDTOHandler ==================

    // Called by Game, sends a player-ready-status dto to the server.
    public void sendReadyStatus(boolean readyStatus) {
        DTOPlayerReadyObject dto = new DTOPlayerReadyObject();
        dto.setPlayerId(mpClient.getPlayerId());
        dto.setPlayerName(mpClient.getUserName());
        dto.setReadyStatus(readyStatus);
        dtoGameDTOHandler.sendDTOPlayerReadyStatus(dto);
    }

    // GameplayActions to server:

    public void sendCardChoice(boolean pay) { // (Choose another card or pay x amount of money. true for pay, false for choose another card)
        DTOatsCardChoice dto = new DTOatsCardChoice();
        dto.setPlayerId(mpClient.getPlayerId());
        dto.setPay(pay);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsCardChoice.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendCardUseGetOutOfJailCard(boolean useCard) {
        DTOatsCardUseGetOutOfJailCard dto = new DTOatsCardUseGetOutOfJailCard();
        dto.setPlayerId(mpClient.getPlayerId());
        dto.setUseCard(useCard);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsCardUseGetOutOfJailCard.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendDieRollTheDice() {
        DTOatsDieRollTheDice dto = new DTOatsDieRollTheDice();
        dto.setPlayerId(mpClient.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsDieRollTheDice.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendGameStatePlayerBankruptcyAcknowledgement() {
        DTOatsGameStatePlayerBankruptcyAcknowledgement dto = new DTOatsGameStatePlayerBankruptcyAcknowledgement();
        dto.setPlayerId(mpClient.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsGameStatePlayerBankruptcyAcknowledgement.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendGameStatePlayerEndTurn() {
        DTOatsGameStatePlayerEndTurn dto = new DTOatsGameStatePlayerEndTurn();
        dto.setPlayerId(mpClient.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsGameStatePlayerEndTurn.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMoneyPayToGetOutOfJail() {
        DTOatsMoneyPayToGetOutOfJail dto = new DTOatsMoneyPayToGetOutOfJail();
        dto.setPlayerId(mpClient.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsMoneyPayToGetOutOfJail.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendPropertyPurchaseDecision(boolean confirmPurchase) {
        DTOatsPropertyPurchaseDecision dto = new DTOatsPropertyPurchaseDecision();
        dto.setPlayerId(mpClient.getPlayerId());
        dto.setConfirmPurchase(confirmPurchase);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsPropertyPurchaseDecision.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendPropertyUpDowngradeDecision(int propertyIndex, boolean upgrade) {
        DTOatsPropertyUpDowngradeDecision dto = new DTOatsPropertyUpDowngradeDecision();
        dto.setPlayerId(mpClient.getPlayerId());
        dto.setPropertyIndex(propertyIndex);
        dto.setUpgrade(upgrade);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(DTOatsPropertyUpDowngradeDecision.class.getSimpleName(), objectMapper.writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // ==================  ClientNetworkListener --> GameNetworkingClientLogic --> Game ==================

    // Called by ClientNetworkListener when client receives gameStart packet.
    public void receiveStartTheGame() {
        game.startTheGame();
    }

    // Called by ClientNetworkListener when client disconnects.
    public void receiveClientDisconnected() {
        game.clientDisconnected();
    }


    // ==================  DTOGameDTOHandler --> GameNetworkingClientLogic --> Game ==================
    // Helper methods

    private Player findPlayerById(String playerId) {
        return game.getPlayers().stream().filter(player -> player.getPlayerId().equals(playerId)).findAny().orElseThrow(() -> new RuntimeException(String.format("Received dto with playerId '%s', which doesn't exist in client's players list", playerId)));
    }
    private Tile findTileByIndex(int tileIndex) {
        return game.getTiles().stream().filter(tile -> tile.getPosition() == tileIndex).findAny().orElseThrow(() -> new RuntimeException(String.format("Received dto with tileIndex '%s', which doesn't exist in client's tiles list", tileIndex)));
    }
    private PropertyTile findPropertyTileByIndex(int propertyIndex) {
        return (PropertyTile) game.getTiles().stream().filter(tile -> tile instanceof PropertyTile).filter(tile -> tile.getPosition() == propertyIndex).findAny().orElseThrow(() -> new RuntimeException(String.format("Received dto with propertyIndex '%s', which doesn't exist in client's tiles list", propertyIndex)));
    }
    private Card findCardByIndex(CardType cardType, int cardIndex) {
        if (cardType.equals(CardType.CHANCE)) {
            return game.getCardsChance().stream().filter(card -> card.getCardIndex() == cardIndex).findAny().orElseThrow(() -> new RuntimeException(String.format("Received dto with cardIndex '%s', which doesn't exist in client's cards list", cardIndex)));
        } else if (cardType.equals(CardType.COMMUNITY_CHEST)) {
            return game.getCardsCommunityChest().stream().filter(card -> card.getCardIndex() == cardIndex).findAny().orElseThrow(() -> new RuntimeException(String.format("Received dto with cardIndex '%s', which doesn't exist in client's cards list", cardIndex)));
        } else {
            throw new RuntimeException("Received dto with unknown CardType");
        }
    }

    // Game data loading
    // Called by DTOGameDTOHandler when client receives game data players.
    public void receiveGameDataPlayers(List<Player> players) {
        game.setPlayers(players);
    }
    // Called by DTOGameDTOHandler when client receives game data tiles.
    public void receiveGameDataTiles(List<Tile> tiles) {
        game.setTiles(tiles);
    }
    // Called by DTOGameDTOHandler when client receives game data chance cards.
    public void receiveGameDataCardsChance(List<Card> cards) {
        game.setCardsChance(cards);
    }
    // Called by DTOGameDTOHandler when client receives game data community chest cards.
    public void receiveGameDataCardsCommunityChest(List<Card> cards) {
        game.setCardsCommunityChest(cards);
    }

    // Called by DTOGameDTOHandler when client receives game player-ready-object dto-s.
    public void receivePlayerReadyStatuses(List<DTOPlayerReadyObject> playerReadyObjects) {
        game.receivedPlayerReadyStatusUpdate(playerReadyObjects);
    }

    // Received gameplay actions: (Called by DTOGameDTOHandler when client receives these gameplay action DTO-s)

    public void receiveCardOfferSwitchACardOrPay(DTOafsCardOfferSwitchACardOrPay dto) {
        game.receiveCardOfferSwitchACardOrPay(findPlayerById(dto.getPlayerId()), dto.getCurrentCardType(), dto.getAnotherCardType(), dto.getAmountToPay());
    }

    public void receiveCardSetPlayerOwnedGetOutOfJailCardsCount(DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount dto) {
        game.receiveCardSetPlayerOwnedGetOutOfJailCardsCount(findPlayerById(dto.getPlayerId()), dto.getGetOutOfJailCardsCount());
    }

    public void receiveCardTakeACard(DTOafsCardTakeACard dto) {
        game.receiveCardTakeACard(findPlayerById(dto.getPlayerId()), findCardByIndex(dto.getCardType(), dto.getCardIndex()));
    }

    public void receiveDieDiceHasBeenRolled(DTOafsDieDiceHasBeenRolled dto) {
        game.receiveDieDiceHasBeenRolled(dto.getDie1Dots(), dto.getDie2Dots());
    }

    public void receiveGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn dto) {
        game.receiveGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(findPlayerById(dto.getPlayerId()), dto.isAllowRollingTheDice(), dto.isAllowPaying(), dto.isAllowUsingACard());
    }

    public void receiveGameStatePlayerWentBankrupt(DTOafsGameStatePlayerWentBankrupt dto) {
        game.receiveGameStatePlayerWentBankrupt(findPlayerById(dto.getPlayerId()));
    }

    public void receiveGameStateSetPlayerTurn(DTOafsGameStateSetPlayerTurn dto) {
        game.receiveGameStateSetPlayerTurn(findPlayerById(dto.getPlayerId()));
    }

    public void receiveGameStateWarnBankruptcy(DTOafsGameStateWarnBankruptcy dto) {
        game.receiveGameStateWarnBankruptcy();
    }

    public void receiveGameStateWinner(DTOafsGameStateWinner dto) {
        game.receiveGameStateWinner(findPlayerById(dto.getPlayerId()));
    }

    public void receiveMoneyGetMoneyFromBank(DTOafsMoneyGetMoneyFromBank dto) {
        game.receiveMoneyGetMoneyFromBank(findPlayerById(dto.getPlayerId()), dto.getPlayerNewBalance(), dto.getReceivedAmount());
    }

    public void receiveMoneyPayMoneyToBank(DTOafsMoneyPayMoneyToBank dto) {
        game.receiveMoneyPayMoneyToBank(findPlayerById(dto.getPlayerId()), dto.getPlayerNewBalance(), dto.getPaidAmount());
    }

    public void receiveMoneyTransferMoneyBetweenPlayers(DTOafsMoneyTransferMoneyBetweenPlayers dto) {
        game.receiveMoneyTransferMoneyBetweenPlayers(findPlayerById(dto.getPayerPlayerId()), dto.getPayerPlayerNewBalance(), findPlayerById(dto.getPayeePlayerId()), dto.getPayeePlayerNewBalance(), dto.getTransferredAmount());
    }

    public void receiveMovingMovePlayerToTile(DTOafsMovingMovePlayerToTile dto) {
        game.receiveMovingMovePlayerToTile(findPlayerById(dto.getPlayerId()), findTileByIndex(dto.getTileIndex()), dto.isMoveBackwards(), dto.isToJail());
    }

    public void receivePropertyOfferToBuy(DTOafsPropertyOfferToBuy dto) {
        game.receivePropertyOfferToBuy(findPlayerById(dto.getPlayerId()), findPropertyTileByIndex(dto.getPropertyIndex()));
    }

    public void receivePropertyRemovePropertyOwnership(DTOafsPropertyRemovePropertyOwnership dto) {
        List<PropertyTile> tilesFromWhichToRemovePropertyOwnership = game.getTiles().stream()
                .filter(tile -> tile instanceof PropertyTile)
                .map(tile -> (PropertyTile) tile)
                .filter(tile -> dto.getPropertyIndices().contains(tile.getPosition()))
                .collect(Collectors.toList());
        game.receivePropertyRemovePropertyOwnership(findPlayerById(dto.getPlayerId()), tilesFromWhichToRemovePropertyOwnership);
    }

    public void receivePropertySetPropertyMortgagedStatus(DTOafsPropertySetPropertyMortgagedStatus dto) {
        game.receivePropertySetPropertyMortgagedStatus(findPropertyTileByIndex(dto.getPropertyIndex()), dto.isMortgagedStatus());
    }

    public void receivePropertySetPropertyOwnership(DTOafsPropertySetPropertyOwnership dto) {
        game.receivePropertySetPropertyOwnership(findPlayerById(dto.getPlayerId()), findPropertyTileByIndex(dto.getPropertyIndex()));
    }

    public void receivePropertyUpDowngradeProperty(DTOafsPropertyUpDowngradeProperty dto) {
        game.receivePropertySetPropertyUpDowngradeProperty(((RegularPropertyTile) findPropertyTileByIndex(dto.getPropertyIndex())), dto.getUpDowngradeToLevel());
    }

    public void receiveLobbyId(String lobbyId) {
        game.receiveLobbyId(lobbyId);
    }
}
