package ee.taltech.iti0200.monopoly.game.controllers;

import java.util.ArrayList;

import ee.taltech.iti0200.monopoly.game.board.Board;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

public class BoardPopulating {

    Board board = new Board();

    public BoardPopulating() {}

    public void generateButtons(ArrayList<BoardButton> board, GridPane grid) {
        
        int i;
        for (i = 0; i < 40; i++) {
            board.add(new BoardButton());
        }
        
        for(BoardButton button : board) {
            
            button.setMaxSize(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
            button.setTile(this.board.getTiles().get(board.indexOf(button)));
        }
        
        populateGridPane(board, grid);
        
        for(ColumnConstraints constraint : grid.getColumnConstraints()) {
            constraint.setFillWidth(true);
        }
        
        for(RowConstraints constraint : grid.getRowConstraints()) {
            constraint.setFillHeight(true);
        }
    }

    private static void populateGridPane(ArrayList<BoardButton> board, GridPane grid) {
        for (int i = 0; i < board.size(); i++) {
            if (i < 10) {
                grid.add(board.get(i), 10 - i, 10);
            } else if (i < 20) {
                grid.add(board.get(i), 0, 20 - i);
            } else if (i < 30) {
                grid.add(board.get(i), i - 20, 0);
            } else if (i < 40) {
                grid.add(board.get(i), 10, i - 30);
            }
        }
    }
}
