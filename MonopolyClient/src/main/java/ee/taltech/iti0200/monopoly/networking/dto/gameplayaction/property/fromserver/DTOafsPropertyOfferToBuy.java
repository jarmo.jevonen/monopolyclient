package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.property.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsPropertyOfferToBuy {
    private int propertyIndex;
    private String playerId;
}
