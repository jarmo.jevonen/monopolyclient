package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.money.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsMoneyPayMoneyToBank {
    private String playerId;
    private double paidAmount;
    private double playerNewBalance;
}
