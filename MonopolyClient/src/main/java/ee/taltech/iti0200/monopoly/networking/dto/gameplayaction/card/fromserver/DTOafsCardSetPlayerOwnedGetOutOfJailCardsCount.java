package ee.taltech.iti0200.monopoly.networking.dto.gameplayaction.card.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount {
    private int getOutOfJailCardsCount;
    private String playerId;
}
