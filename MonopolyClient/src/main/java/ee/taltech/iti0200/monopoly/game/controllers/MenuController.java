package ee.taltech.iti0200.monopoly.game.controllers;

import ee.taltech.iti0200.monopoly.networking.dto.DTOPlayerReadyObject;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.List;

public class MenuController extends BaseController {
    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField ipAddressTextField;
    @FXML
    private TextField portTextField;
    @FXML
    private TextField lobbyIdTextField;
    @FXML
    private Button createLobbyButton;
    @FXML
    private Button joinLobbyButton;
    @FXML
    private Button addAIPlayerButton;
    @FXML
    private CheckBox playerReadyCheckbox;

    @FXML
    private Label lobbyIdLabel;
    @FXML
    private Label lobbyPlayer1UsernameLabel;
    @FXML
    private CheckBox lobbyPlayer1ReadyCheckbox;
    @FXML
    private Label lobbyPlayer2UsernameLabel;
    @FXML
    private CheckBox lobbyPlayer2ReadyCheckbox;
    @FXML
    private Label lobbyPlayer3UsernameLabel;
    @FXML
    private CheckBox lobbyPlayer3ReadyCheckbox;
    @FXML
    private Label lobbyPlayer4UsernameLabel;
    @FXML
    private CheckBox lobbyPlayer4ReadyCheckbox;
    @FXML
    private Label lobbyPlayer5UsernameLabel;
    @FXML
    private CheckBox lobbyPlayer5ReadyCheckbox;
    @FXML
    private Label lobbyPlayer6UsernameLabel;
    @FXML
    private CheckBox lobbyPlayer6ReadyCheckbox;


    private List<Label> lobbyPlayerUsernameLabels;
    private List<CheckBox> lobbyPlayerReadyCheckboxes;

    // Collects ip, port and username fields and calls connectToServer() with them.
    public void createLobbyButtonOnClick() {
        String userName = usernameTextField.textProperty().getValue();
        String ipAddress = ipAddressTextField.textProperty().getValue();
        String port = portTextField.textProperty().getValue();
        if (port.matches("\\d+")) {
            game.connectToServer(ipAddress, Integer.parseInt(port), userName, "", false);
        } else {
            portTextField.textProperty().setValue("");
        }
    }

    public void joinLobbyButtonOnClick() {
        String userName = usernameTextField.textProperty().getValue();
        String ipAddress = ipAddressTextField.textProperty().getValue();
        String port = portTextField.textProperty().getValue();
        String lobbyId = lobbyIdTextField.textProperty().getValue();
        if (port.matches("\\d+") && !lobbyId.isBlank()) {
            game.connectToServer(ipAddress, Integer.parseInt(port), userName, lobbyId, true);
        } else {
            portTextField.textProperty().setValue("");
        }
    }

    public void addAIPlayerButtonOnClick() {
        game.addAIPlayer();
    }

    // Enables or disables the set-ready button based on whether the client is connected to the server.
    public void setDisabledPlayerReadyCheckBox(boolean bool) {
        playerReadyCheckbox.setDisable(bool);
    }


    // Refreshes the players list with ready-statuses.
    public void refreshPlayersStatus(List<DTOPlayerReadyObject> dtoPlayerReadyObjects) {
        Platform.runLater(() -> {
            addAIPlayerButton.setDisable(false);
            addAIPlayerButton.setVisible(true);
            for (int i = 0; i < 6; i++) {
                if (i >= dtoPlayerReadyObjects.size()) {
                    lobbyPlayerUsernameLabels.get(i).setText("");
                    lobbyPlayerReadyCheckboxes.get(i).setSelected(false);
                    lobbyPlayerReadyCheckboxes.get(i).setVisible(false);
                }
                else {
                    lobbyPlayerUsernameLabels.get(i).setText(dtoPlayerReadyObjects.get(i).getPlayerName());
                    lobbyPlayerReadyCheckboxes.get(i).setSelected(dtoPlayerReadyObjects.get(i).isReadyStatus());
                    lobbyPlayerReadyCheckboxes.get(i).setVisible(true);
                }
            }
        });
    }


    // Sets up the UI. Is called by JavaFX via Dependency Injection.
    @FXML
    public void initialize() {
        lobbyPlayerUsernameLabels = List.of(lobbyPlayer1UsernameLabel, lobbyPlayer2UsernameLabel, lobbyPlayer3UsernameLabel, lobbyPlayer4UsernameLabel, lobbyPlayer5UsernameLabel, lobbyPlayer6UsernameLabel);
        lobbyPlayerReadyCheckboxes = List.of(lobbyPlayer1ReadyCheckbox, lobbyPlayer2ReadyCheckbox, lobbyPlayer3ReadyCheckbox, lobbyPlayer4ReadyCheckbox, lobbyPlayer5ReadyCheckbox, lobbyPlayer6ReadyCheckbox);

        // Add an event listener for the player-ready-checkbox as there is no native onToggle() method.
        playerReadyCheckbox.setOnAction(event -> {
            game.playerReadyStatusChanged(playerReadyCheckbox.isSelected());
        });
    }

    public void showLobbyId(String lobbyId) {
        Platform.runLater(() -> {
            lobbyIdLabel.setText("Mängu ID: " + lobbyId);
            joinLobbyButton.setDisable(true);
            createLobbyButton.setDisable(true);
        });
    }
}
