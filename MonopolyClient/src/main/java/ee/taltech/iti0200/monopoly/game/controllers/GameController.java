package ee.taltech.iti0200.monopoly.game.controllers;

import com.jfoenix.controls.JFXDrawer;
import ee.taltech.iti0200.monopoly.game.board.tile.*;
import ee.taltech.iti0200.monopoly.game.player.Player;
import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import lombok.Getter;

import java.text.DecimalFormat;
import java.util.*;

import static javafx.scene.paint.Color.BLACK;

public class GameController extends BaseController {
    private AudioClip audioClip;
    private Map<String, Circle> playerPiecesMap;
    private Map<Integer, int[]> tileIndexToCoordinatesMapper;
    private Map<List<Integer>, Integer> tileCoordinatesToIndexMapper;
    private Map<String, AnchorPane> playerInfoAnchorPanes;
    private Map<Integer, Rectangle> boardPropertyTileOwnershipRectanglesMap;
    private Map<String, VBox> playerInfoBalanceVBoxesMap;
    private Map<String, FadeTransition> fadeInOutBalanceIncreaseTransitionsMap;
    private Map<String, FadeTransition> fadeInOutBalanceDecreaseTransitionsMap;
    private DecimalFormat decimalFormat;
    private InnerShadow innerShadow;
    private Image houseImage;
    private Image hotelImage;

    @FXML
    private Label gameControllerLabel;
    @FXML
    private Button rollTheDiceButton;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private GridPane boardPane;
    @FXML
    private AnchorPane jailTile;
    @FXML
    private Button rollDiceButton;
    @FXML
    private Circle playerPiece1Circle;
    @FXML
    private Circle playerPiece2Circle;
    @FXML
    private Circle playerPiece3Circle;
    @FXML
    private Circle playerPiece4Circle;
    @FXML
    private Circle playerPiece5Circle;
    @FXML
    private Circle playerPiece6Circle;
    @FXML
    private ImageView dice0;
    @FXML
    private ImageView dice1;

    @FXML
    private AnchorPane boardAnchor;

    @Getter
    @FXML
    private HBox gameControlsHBox;




    @FXML
    private AnchorPane propertyCardCloseUpAnchorPane;

    @FXML
    private AnchorPane propertyCardCloseUpInteractionsAnchorPane;

    @FXML
    private Label propertyCardCloseUpInteractionsBuildingCountLabel;

    @FXML
    private AnchorPane winnerAnchorPane;

    @FXML
    private Label winnerLabel;

    // ===================================  GET OUT OF JAIL MENU ========================================
    @FXML
    private AnchorPane getOutOfJailOptionsAnchorPane;

    @FXML
    private ImageView diceRollImageViewWhite;

    @FXML
    private ImageView payOutOfJailImageViewWhite;

    @FXML
    private ImageView getOutOfJailCardImageViewWhite;

    // =============================================  PLAYERS INFO =====================================

    @FXML
    private VBox playersInfoVBox;
    @FXML
    private VBox playerInfoPlayer1BalanceVBox;
    @FXML
    private VBox playerInfoPlayer2BalanceVBox;
    @FXML
    private VBox playerInfoPlayer3BalanceVBox;
    @FXML
    private VBox playerInfoPlayer4BalanceVBox;
    @FXML
    private VBox playerInfoPlayer5BalanceVBox;
    @FXML
    private VBox playerInfoPlayer6BalanceVBox;


    // =============================================  CARD =====================================

    @FXML
    private AnchorPane cardAnchorPane;
    @FXML
    private Label cardTitleLabel;
    @FXML
    private Label cardPlayerNameLabel;
    @FXML
    private Label cardTextRow1Label;
    @FXML
    private Label cardTextRow2Label;
    @FXML
    private Label cardTextRow3Label;
    @FXML
    private Label cardTextRow4Label;
    @FXML
    private ImageView cardChoiceSwitchImageView;
    @FXML
    private ImageView cardOkImageView;
    @FXML
    private ImageView cardChoicePayImageView;

    // =============================================  PURCHASE DRAWER =====================================

    @FXML
    private JFXDrawer propertyPurchaseDrawer;
    @FXML
    private AnchorPane propertyPurchaseDrawerSidePane;
    @FXML
    private AnchorPane propertyCardPurchaseAnchorPane;


    // ======================================  BOARD PROPERTY OWNERSHIP RECTANGLES =====================================

    @FXML
    private Rectangle boardPropertyTileOwnership01Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership03Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership05Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership06Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership08Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership09Rectangle;

    @FXML
    private Rectangle boardPropertyTileOwnership11Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership12Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership13Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership14Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership15Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership16Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership18Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership19Rectangle;

    @FXML
    private Rectangle boardPropertyTileOwnership21Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership23Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership24Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership25Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership26Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership27Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership28Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership29Rectangle;

    @FXML
    private Rectangle boardPropertyTileOwnership31Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership32Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership34Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership35Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership37Rectangle;
    @FXML
    private Rectangle boardPropertyTileOwnership39Rectangle;


    // ======================================  BANKRUPTCY WARNING =====================================
    @FXML
    private AnchorPane bankruptcyWarningAnchorPane;




    public void setUpGameBoard(List<Player> players) {
        decimalFormat = new DecimalFormat("#.#");
        audioClip = new AudioClip(getClass().getResource("/ee/taltech/iti0200/monopoly/game/audio/turnalert.wav").toExternalForm());
        innerShadow = new InnerShadow(BlurType.THREE_PASS_BOX, Color.YELLOW, 18, 0.54, 0, 0);
        innerShadow.setWidth(35.6);
        innerShadow.setHeight(38.8);
        List<Circle> playerPieces = new ArrayList<>(List.of(playerPiece1Circle, playerPiece2Circle, playerPiece3Circle, playerPiece4Circle, playerPiece5Circle, playerPiece6Circle));
        List<VBox> playerInfoBalanceVBoxes = new ArrayList<>(List.of(playerInfoPlayer1BalanceVBox, playerInfoPlayer2BalanceVBox, playerInfoPlayer3BalanceVBox, playerInfoPlayer4BalanceVBox, playerInfoPlayer5BalanceVBox, playerInfoPlayer6BalanceVBox));
        fadeInOutBalanceIncreaseTransitionsMap = new HashMap<>();
        fadeInOutBalanceDecreaseTransitionsMap = new HashMap<>();
        playerInfoBalanceVBoxesMap = new HashMap<>();
        playerPiecesMap = new HashMap<>();
        playerInfoAnchorPanes = new HashMap<>();
        /*houseImage = new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/gameView/houses/house.png"));
        hotelImage = new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/gameView/houses/hotel.png"));*/
        houseImage = new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/gameView/houses/house.png"));
        hotelImage = new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/gameView/houses/hotel.png"));

        dice0.setVisible(false);
        dice1.setVisible(false);

        for (int i = 0; i < players.size(); i++) {
            playerPiecesMap.put(players.get(i).getPlayerId(), playerPieces.get(i));
            playerPieces.get(i).setVisible(true);
            playerInfoBalanceVBoxesMap.put(players.get(i).getPlayerId(), playerInfoBalanceVBoxes.get(i));
            playerInfoAnchorPanes.put(players.get(i).getPlayerId(), (AnchorPane) playersInfoVBox.getChildren().get(i));
            ((Rectangle) playerInfoAnchorPanes.get(players.get(i).getPlayerId()).getChildren().get(0)).setFill(playerPieces.get(i).getFill());
            ((Label) playerInfoAnchorPanes.get(players.get(i).getPlayerId()).getChildren().get(1)).textProperty().setValue(players.get(i).getName());
            ((Label) playerInfoBalanceVBoxesMap.get(players.get(i).getPlayerId()).getChildren().get(1)).textProperty().setValue(decimalFormat.format(players.get(i).getBalance()));
            fadeInOutBalanceIncreaseTransitionsMap.put(players.get(i).getPlayerId(), new FadeTransition(Duration.millis(2500)));
            fadeInOutBalanceDecreaseTransitionsMap.put(players.get(i).getPlayerId(), new FadeTransition(Duration.millis(2500)));
        }
        for (int i = players.size(); i < 6; i++) {
            playersInfoVBox.getChildren().get(i).setDisable(true);
            playersInfoVBox.getChildren().get(i).setVisible(false);
        }

        propertyPurchaseDrawer.setOverLayVisible(false);
        propertyPurchaseDrawer.setSidePane(propertyPurchaseDrawerSidePane);

        tileIndexToCoordinatesMapper = Map.ofEntries(
                new AbstractMap.SimpleEntry<>(0, new int[]{10, 10}),
                new AbstractMap.SimpleEntry<>(1, new int[]{10, 9}),
                new AbstractMap.SimpleEntry<>(2, new int[]{10, 8}),
                new AbstractMap.SimpleEntry<>(3, new int[]{10, 7}),
                new AbstractMap.SimpleEntry<>(4, new int[]{10, 6}),
                new AbstractMap.SimpleEntry<>(5, new int[]{10, 5}),
                new AbstractMap.SimpleEntry<>(6, new int[]{10, 4}),
                new AbstractMap.SimpleEntry<>(7, new int[]{10, 3}),
                new AbstractMap.SimpleEntry<>(8, new int[]{10, 2}),
                new AbstractMap.SimpleEntry<>(9, new int[]{10, 1}),
                new AbstractMap.SimpleEntry<>(10, new int[]{10, 0}),
                new AbstractMap.SimpleEntry<>(11, new int[]{9, 0}),
                new AbstractMap.SimpleEntry<>(12, new int[]{8, 0}),
                new AbstractMap.SimpleEntry<>(13, new int[]{7, 0}),
                new AbstractMap.SimpleEntry<>(14, new int[]{6, 0}),
                new AbstractMap.SimpleEntry<>(15, new int[]{5, 0}),
                new AbstractMap.SimpleEntry<>(16, new int[]{4, 0}),
                new AbstractMap.SimpleEntry<>(17, new int[]{3, 0}),
                new AbstractMap.SimpleEntry<>(18, new int[]{2, 0}),
                new AbstractMap.SimpleEntry<>(19, new int[]{1, 0}),
                new AbstractMap.SimpleEntry<>(20, new int[]{0, 0}),
                new AbstractMap.SimpleEntry<>(21, new int[]{0, 1}),
                new AbstractMap.SimpleEntry<>(22, new int[]{0, 2}),
                new AbstractMap.SimpleEntry<>(23, new int[]{0, 3}),
                new AbstractMap.SimpleEntry<>(24, new int[]{0, 4}),
                new AbstractMap.SimpleEntry<>(25, new int[]{0, 5}),
                new AbstractMap.SimpleEntry<>(26, new int[]{0, 6}),
                new AbstractMap.SimpleEntry<>(27, new int[]{0, 7}),
                new AbstractMap.SimpleEntry<>(28, new int[]{0, 8}),
                new AbstractMap.SimpleEntry<>(29, new int[]{0, 9}),
                new AbstractMap.SimpleEntry<>(30, new int[]{0, 10}),
                new AbstractMap.SimpleEntry<>(31, new int[]{1, 10}),
                new AbstractMap.SimpleEntry<>(32, new int[]{2, 10}),
                new AbstractMap.SimpleEntry<>(33, new int[]{3, 10}),
                new AbstractMap.SimpleEntry<>(34, new int[]{4, 10}),
                new AbstractMap.SimpleEntry<>(35, new int[]{5, 10}),
                new AbstractMap.SimpleEntry<>(36, new int[]{6, 10}),
                new AbstractMap.SimpleEntry<>(37, new int[]{7, 10}),
                new AbstractMap.SimpleEntry<>(38, new int[]{8, 10}),
                new AbstractMap.SimpleEntry<>(39, new int[]{9, 10})
        );

        tileCoordinatesToIndexMapper = Map.ofEntries(
                new AbstractMap.SimpleEntry<>(List.of(10, 10), 0),
                new AbstractMap.SimpleEntry<>(List.of(10, 9), 1),
                new AbstractMap.SimpleEntry<>(List.of(10, 8), 2),
                new AbstractMap.SimpleEntry<>(List.of(10, 7), 3),
                new AbstractMap.SimpleEntry<>(List.of(10, 6), 4),
                new AbstractMap.SimpleEntry<>(List.of(10, 5), 5),
                new AbstractMap.SimpleEntry<>(List.of(10, 4), 6),
                new AbstractMap.SimpleEntry<>(List.of(10, 3), 7),
                new AbstractMap.SimpleEntry<>(List.of(10, 2), 8),
                new AbstractMap.SimpleEntry<>(List.of(10, 1), 9),
                new AbstractMap.SimpleEntry<>(List.of(10, 0), 10),
                new AbstractMap.SimpleEntry<>(List.of(9, 0), 11),
                new AbstractMap.SimpleEntry<>(List.of(8, 0), 12),
                new AbstractMap.SimpleEntry<>(List.of(7, 0), 13),
                new AbstractMap.SimpleEntry<>(List.of(6, 0), 14),
                new AbstractMap.SimpleEntry<>(List.of(5, 0), 15),
                new AbstractMap.SimpleEntry<>(List.of(4, 0), 16),
                new AbstractMap.SimpleEntry<>(List.of(3, 0), 17),
                new AbstractMap.SimpleEntry<>(List.of(2, 0), 18),
                new AbstractMap.SimpleEntry<>(List.of(1, 0), 19),
                new AbstractMap.SimpleEntry<>(List.of(0, 0), 20),
                new AbstractMap.SimpleEntry<>(List.of(0, 1), 21),
                new AbstractMap.SimpleEntry<>(List.of(0, 2), 22),
                new AbstractMap.SimpleEntry<>(List.of(0, 3), 23),
                new AbstractMap.SimpleEntry<>(List.of(0, 4), 24),
                new AbstractMap.SimpleEntry<>(List.of(0, 5), 25),
                new AbstractMap.SimpleEntry<>(List.of(0, 6), 26),
                new AbstractMap.SimpleEntry<>(List.of(0, 7), 27),
                new AbstractMap.SimpleEntry<>(List.of(0, 8), 28),
                new AbstractMap.SimpleEntry<>(List.of(0, 9), 29),
                new AbstractMap.SimpleEntry<>(List.of(0, 10), 30),
                new AbstractMap.SimpleEntry<>(List.of(1, 10), 31),
                new AbstractMap.SimpleEntry<>(List.of(2, 10), 32),
                new AbstractMap.SimpleEntry<>(List.of(3, 10), 33),
                new AbstractMap.SimpleEntry<>(List.of(4, 10), 34),
                new AbstractMap.SimpleEntry<>(List.of(5, 10), 35),
                new AbstractMap.SimpleEntry<>(List.of(6, 10), 36),
                new AbstractMap.SimpleEntry<>(List.of(7, 10), 37),
                new AbstractMap.SimpleEntry<>(List.of(8, 10), 38),
                new AbstractMap.SimpleEntry<>(List.of(9, 10), 39)
        );

        initializeBoardPaneButtons();

        boardPropertyTileOwnershipRectanglesMap = Map.ofEntries(
                new AbstractMap.SimpleEntry<>(1, boardPropertyTileOwnership01Rectangle),
                new AbstractMap.SimpleEntry<>(3, boardPropertyTileOwnership03Rectangle),
                new AbstractMap.SimpleEntry<>(5, boardPropertyTileOwnership05Rectangle),
                new AbstractMap.SimpleEntry<>(6, boardPropertyTileOwnership06Rectangle),
                new AbstractMap.SimpleEntry<>(8, boardPropertyTileOwnership08Rectangle),
                new AbstractMap.SimpleEntry<>(9, boardPropertyTileOwnership09Rectangle),

                new AbstractMap.SimpleEntry<>(11, boardPropertyTileOwnership11Rectangle),
                new AbstractMap.SimpleEntry<>(12, boardPropertyTileOwnership12Rectangle),
                new AbstractMap.SimpleEntry<>(13, boardPropertyTileOwnership13Rectangle),
                new AbstractMap.SimpleEntry<>(14, boardPropertyTileOwnership14Rectangle),
                new AbstractMap.SimpleEntry<>(15, boardPropertyTileOwnership15Rectangle),
                new AbstractMap.SimpleEntry<>(16, boardPropertyTileOwnership16Rectangle),
                new AbstractMap.SimpleEntry<>(18, boardPropertyTileOwnership18Rectangle),
                new AbstractMap.SimpleEntry<>(19, boardPropertyTileOwnership19Rectangle),

                new AbstractMap.SimpleEntry<>(21, boardPropertyTileOwnership21Rectangle),
                new AbstractMap.SimpleEntry<>(23, boardPropertyTileOwnership23Rectangle),
                new AbstractMap.SimpleEntry<>(24, boardPropertyTileOwnership24Rectangle),
                new AbstractMap.SimpleEntry<>(25, boardPropertyTileOwnership25Rectangle),
                new AbstractMap.SimpleEntry<>(26, boardPropertyTileOwnership26Rectangle),
                new AbstractMap.SimpleEntry<>(27, boardPropertyTileOwnership27Rectangle),
                new AbstractMap.SimpleEntry<>(28, boardPropertyTileOwnership28Rectangle),
                new AbstractMap.SimpleEntry<>(29, boardPropertyTileOwnership29Rectangle),

                new AbstractMap.SimpleEntry<>(31, boardPropertyTileOwnership31Rectangle),
                new AbstractMap.SimpleEntry<>(32, boardPropertyTileOwnership32Rectangle),
                new AbstractMap.SimpleEntry<>(34, boardPropertyTileOwnership34Rectangle),
                new AbstractMap.SimpleEntry<>(35, boardPropertyTileOwnership35Rectangle),
                new AbstractMap.SimpleEntry<>(37, boardPropertyTileOwnership37Rectangle),
                new AbstractMap.SimpleEntry<>(39, boardPropertyTileOwnership39Rectangle)
        );
    }

    // ====================================  BUTTONS FOR TILES ON BOARD ============================================

    private void initializeBoardPaneButtons() {
        int numCols = 11 ;
        int numRows = 11 ;

        for (int i = 0 ; i < numCols ; i++) {
            ColumnConstraints colConstraints = new ColumnConstraints();
            colConstraints.setHgrow(Priority.SOMETIMES);
            boardPane.getColumnConstraints().add(colConstraints);
        }

        for (int i = 0 ; i < numRows ; i++) {
            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setVgrow(Priority.SOMETIMES);
            boardPane.getRowConstraints().add(rowConstraints);
        }

        for (int i = 0; i < 40; i++) {
            int[] tileCoords = tileIndexToCoordinatesMapper.get(i);

            addBoxForHouses(tileCoords[0], tileCoords[1]);
            addPane(tileCoords[0], tileCoords[1]);
        }

        // Not needed as we only use edge tiles on the game board grid.
        /*for (int i = 0 ; i < numCols ; i++) {
            for (int j = 0; j < numRows; j++) {
                addPane(i, j);
            }
        }*/
    }

    private void addPane(int rowIndex, int colIndex) {
        Pane pane = new Pane();
        pane.setOnMouseClicked(e -> {
            showPropertyTileCardCloseUp(tileCoordinatesToIndexMapper.get(List.of(rowIndex, colIndex)));
        });
        boardPane.add(pane, colIndex, rowIndex);
    }

    private void addBoxForHouses(int rowIndex, int colIndex) {
        if (colIndex == 0 || colIndex == 10) {
            VBox vBox = new VBox();
            if (colIndex == 0) {
                vBox.setAlignment(Pos.CENTER_RIGHT);
                vBox.setPadding(new Insets(0, 6, 0, 0));
            } else {
                vBox.setAlignment(Pos.CENTER_LEFT);
                vBox.setPadding(new Insets(0, 0, 0, 6));
            }
            boardPane.add(vBox, colIndex, rowIndex);
        } else {
            HBox hBox = new HBox();
            if (rowIndex == 0) {
                hBox.setAlignment(Pos.BOTTOM_CENTER);
                hBox.setPadding(new Insets(0, 0, 6, 0));
            } else {
                hBox.setAlignment(Pos.TOP_CENTER);
                hBox.setPadding(new Insets(6, 0, 0, 0));
            }
            boardPane.add(hBox, colIndex, rowIndex);
        }
    }

    public void updateBuildingsCountOnTile(int tileIndex, int buildingsCount) {
        Platform.runLater(() -> setBuildingsCountOnBoard(tileIndexToCoordinatesMapper.get(tileIndex)[0], tileIndexToCoordinatesMapper.get(tileIndex)[1], buildingsCount));
    }

    private void setBuildingsCountOnBoard(int rowIndex, int colIndex, int buildingsCount) {
        Node result = null;
        ObservableList<Node> children = boardPane.getChildren();
        for (Node node : children) {
            if(GridPane.getRowIndex(node) == rowIndex && GridPane.getColumnIndex(node) == colIndex) {
                result = node;
                break;
            }
        }

        if (result instanceof VBox) {
            ((VBox) result).getChildren().clear();
            if (buildingsCount == 5) {
                ImageView imageView = new ImageView(hotelImage);
                imageView.setFitHeight(17);
                imageView.setFitWidth(17);

                ((VBox) result).getChildren().add(imageView);
            } else {
                for (int i = 0; i < buildingsCount; i++) {

                    ImageView imageView = new ImageView(houseImage);
                    imageView.setFitHeight(16);
                    imageView.setFitWidth(16);

                    ((VBox) result).getChildren().add(imageView);
                }
            }

        } else if (result instanceof HBox) {
            ((HBox) result).getChildren().clear();
            if (buildingsCount == 5) {
                ImageView imageView = new ImageView(hotelImage);
                imageView.setFitHeight(16);
                imageView.setFitWidth(16);
                ((HBox) result).getChildren().add(imageView);
            } else {
                for (int i = 0; i < buildingsCount; i++) {
                    ImageView imageView = new ImageView(houseImage);
                    imageView.setFitHeight(16);
                    imageView.setFitWidth(16);
                    ((HBox) result).getChildren().add(imageView);
                }
            }

        }
    }


    private void showPropertyTileCardCloseUp(int tileIndex) {
        if (game.getTiles().stream().filter(tile -> tile instanceof PropertyTile).mapToInt(Tile::getPosition).anyMatch(propTileIndex -> propTileIndex == tileIndex)) {
            PropertyTile propertyTile = (PropertyTile) game.getTiles().get(tileIndex);
            game.setCurrentlyOpenedCloseUpPropertyTile(tileIndex);
            fillPropertyTileCard(propertyCardCloseUpAnchorPane, propertyTile);

            propertyCardCloseUpAnchorPane.setVisible(true);
            if ((propertyTile.getOwner() != null && propertyTile.getOwner().getPlayerId().equals(game.getGameNetworkingClientLogic().getMpClient().getPlayerId()))) {
                propertyCardCloseUpInteractionsBuildingCountLabel.textProperty().setValue(game.getPropertyTileBuildingsCountOrMortgageString(tileIndex));
                propertyCardCloseUpInteractionsAnchorPane.setVisible(true);
            } else {
                propertyCardCloseUpInteractionsAnchorPane.setVisible(false);
            }
        }
    }

    @FXML
    private void propertyCardCloseUpAnchorPaneOnClick() { // For hiding property card close-up
        game.setCurrentlyOpenedCloseUpPropertyTile(0);
        propertyCardCloseUpAnchorPane.setVisible(false);
        propertyCardCloseUpInteractionsAnchorPane.setVisible(false);
    }

    public void setPropertyCardCloseUpInteractionsBuildingCountLabel(String buildingsCountString) {
        Platform.runLater(() -> propertyCardCloseUpInteractionsBuildingCountLabel.textProperty().setValue(buildingsCountString));
    }

    // Used to fill propertyTileCards' close-ups or purchaseDrawers. Later use for displaying cards in owned-cards overview.
    private void fillPropertyTileCard(AnchorPane propertyAnchorPaneToFill, PropertyTile propertyTile) {

        propertyAnchorPaneToFill.getChildren().get(1).setVisible(false);
        propertyAnchorPaneToFill.getChildren().get(2).setVisible(false);
        propertyAnchorPaneToFill.getChildren().get(3).setVisible(false);
        matchPropertyTileCardToCorrectBackground((ImageView) propertyAnchorPaneToFill.getChildren().get(0), propertyTile);

        if (propertyTile instanceof UtilityPropertyTile) {
            AnchorPane propertyCardCloseUpInfoAnchorPane = (AnchorPane) propertyAnchorPaneToFill.getChildren().get(2);
            ((Label) propertyCardCloseUpInfoAnchorPane.getChildren().get(0)).textProperty().setValue(propertyTile.getTileName().toUpperCase()); // Close up utility card title
            ((Label) ((HBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).textProperty().setValue(decimalFormat.format(propertyTile.getMortgageValue()) + " pelmeeni"); // Close up utility mortgage value
            propertyCardCloseUpInfoAnchorPane.setVisible(true);
        } else if (propertyTile instanceof RailroadPropertyTile) {
            RailroadPropertyTile railroadPropertyTile = (RailroadPropertyTile) propertyTile;
            AnchorPane propertyCardCloseUpInfoAnchorPane = (AnchorPane) propertyAnchorPaneToFill.getChildren().get(1);
            ((Label) propertyCardCloseUpInfoAnchorPane.getChildren().get(0)).textProperty().setValue(propertyTile.getTileName().toUpperCase()); // Close up railroad card title

            ((Label) ((VBox) ((HBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(1)).getChildren().get(1)).getChildren().get(0)).textProperty().setValue(decimalFormat.format(railroadPropertyTile.getRentWithOneStation()) + " pelmeeni");
            ((Label) ((VBox) ((HBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(1)).getChildren().get(1)).getChildren().get(1)).textProperty().setValue(decimalFormat.format(railroadPropertyTile.getRentWithTwoStations()) + " pelmeeni");
            ((Label) ((VBox) ((HBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(1)).getChildren().get(1)).getChildren().get(2)).textProperty().setValue(decimalFormat.format(railroadPropertyTile.getRentWithThreeStations()) + " pelmeeni");
            ((Label) ((VBox) ((HBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(1)).getChildren().get(1)).getChildren().get(3)).textProperty().setValue(decimalFormat.format(railroadPropertyTile.getRentWithFourStations()) + " pelmeeni");
            ((Label) ((HBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).textProperty().setValue(decimalFormat.format(propertyTile.getMortgageValue()) + " pelmeeni"); // Close up utility mortgage value
            propertyCardCloseUpInfoAnchorPane.setVisible(true);
        } else if (propertyTile instanceof RegularPropertyTile) {
            RegularPropertyTile regularPropertyTile = (RegularPropertyTile) propertyTile;
            AnchorPane propertyCardCloseUpInfoAnchorPane = (AnchorPane) propertyAnchorPaneToFill.getChildren().get(3);
            ((Rectangle) propertyCardCloseUpInfoAnchorPane.getChildren().get(0)).setFill(matchRegularPropertyTileCardColorToColorCode(regularPropertyTile.getPropertyColor()));
            ((Label) propertyCardCloseUpInfoAnchorPane.getChildren().get(1)).textProperty().setValue(propertyTile.getTileName().toUpperCase()); // Close up regular card title
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).getChildren().get(1)).getChildren().get(1)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getBaseRent()));
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).getChildren().get(1)).getChildren().get(2)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getRentWithOneHouse()));
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).getChildren().get(1)).getChildren().get(3)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getRentWithTwoHouses()));
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).getChildren().get(1)).getChildren().get(4)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getRentWithThreeHouses()));
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).getChildren().get(1)).getChildren().get(5)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getRentWithFourHouses()));
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(1)).getChildren().get(1)).getChildren().get(6)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getRentWithAHotel()));

            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(5)).getChildren().get(1)).getChildren().get(1)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getUpgradePrice()) + " pelmeeni");
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(5)).getChildren().get(1)).getChildren().get(2)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getUpgradePrice()) + " pelmeeni");
            ((Label) ((VBox) ((HBox) ((VBox) propertyCardCloseUpInfoAnchorPane.getChildren().get(2)).getChildren().get(5)).getChildren().get(1)).getChildren().get(5)).textProperty().setValue(decimalFormat.format(regularPropertyTile.getMortgageValue()) + " pelmeeni");

            propertyCardCloseUpInfoAnchorPane.setVisible(true);

        } else {
            throw new RuntimeException(String.format("Error occurred while trying to set label info for propertyTile '%s'.", propertyTile));
        }
    }


    @FXML
    private void imageViewOnClick(MouseEvent mouseEvent) {
        ImageView imageView = ((ImageView) mouseEvent.getTarget());
        if (imageView.getId().equals("rollDiceBtnImageView")) {
            propertyPurchaseDrawer.close();
            game.rollTheDiceButtonOnClick();
        } else if (imageView.getId().equals("endTurnBtnImageView")) {
            dice0.setVisible(false);
            dice1.setVisible(false);
            game.endTurn();
        } else if (imageView.getId().equals("proceedEndTurnImageViewWhite")) {
            game.goBankrupt();
            bankruptcyWarningAnchorPane.setVisible(false);
        } else if (imageView.getId().equals("cancelEndTurnImageViewWhite")) {
            bankruptcyWarningAnchorPane.setVisible(false);
        } else if (imageView.getId().equals("propertyCardCloseUpInteractionsMinusImageView")) {
            game.setPropertyPropertyUpDowngradeDecision(false);
        } else if (imageView.getId().equals("propertyCardCloseUpInteractionsPlusImageView")) {
            game.setPropertyPropertyUpDowngradeDecision(true);
        } else if (imageView.getId().equals("diceRollImageViewWhite")) {
            game.rollTheDiceButtonOnClick();
            getOutOfJailOptionsAnchorPane.setVisible(false);
        } else if (imageView.getId().equals("payOutOfJailImageViewWhite")) {
            game.payToGetOutOfJail();
            getOutOfJailOptionsAnchorPane.setVisible(false);
        } else if (imageView.getId().equals("getOutOfJailCardImageViewWhite")) {
            game.useGetOutOfJailCard();
            getOutOfJailOptionsAnchorPane.setVisible(false);
        } else if (imageView.getId().equals("cardChoiceSwitchImageView")) {
            game.chooseSwitchCardOrPay(false);
            cardViewHide();
        } else if (imageView.getId().equals("cardOkImageView")) {
            cardViewHide();
        } else if (imageView.getId().equals("cardChoicePayImageView")) {
            game.chooseSwitchCardOrPay(true);
            cardViewHide();
        }
    }

    @FXML
    private void imageViewOnMouseEntered(MouseEvent mouseEvent) {
        ImageView imageView = ((ImageView) mouseEvent.getTarget());
        imageView.setOpacity(0.5);
    }

    @FXML
    private void imageViewOnMouseExited(MouseEvent mouseEvent) {
        ImageView imageView = ((ImageView) mouseEvent.getTarget());
        imageView.setOpacity(1);
    }

    @FXML
    private void imageViewOnMousePressed(MouseEvent mouseEvent) {
        ImageView imageView = ((ImageView) mouseEvent.getTarget());
        imageView.setOpacity(0.25);
    }

    @FXML
    private void imageViewOnMouseReleased(MouseEvent mouseEvent) {
        ImageView imageView = ((ImageView) mouseEvent.getTarget());
        imageView.setOpacity(0.5);
    }

    public void showDiceRoll(int die1Dots, int die2Dots) {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {

                dice0.setVisible(true);
                dice1.setVisible(true);

                RotateTransition rt = new RotateTransition(Duration.seconds(1), dice0);
                rt.setFromAngle(0);
                rt.setToAngle(360);
                rt.setOnFinished(e -> {
                    dice0.setImage(new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/gameView/icons/dice" + die1Dots + ".png")));
                });
                rt.play();

                rt = new RotateTransition(Duration.seconds(1), dice1);
                rt.setFromAngle(0);
                rt.setToAngle(360);
                rt.setOnFinished(e -> {
                    dice1.setImage(new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/gameView/icons/dice" + die2Dots + ".png")));
                });
                rt.play();
            }
        });
    }

    // =============================================  PURCHASE DRAWER  ===============================================

    public void propertyOfferToBuy(PropertyTile propertyTile) {
        Platform.runLater(() -> {
            ((Label) propertyPurchaseDrawerSidePane.getChildren().get(1)).textProperty().setValue(String.format("Hind %s pelmeeni", decimalFormat.format(propertyTile.getPrice())));
            fillPropertyTileCard(propertyCardPurchaseAnchorPane, propertyTile);
            propertyPurchaseDrawer.open();
            propertyCardPurchaseAnchorPane.setVisible(true);
        });
    }

    public void propertyPurchaseDrawerEnable() {
        propertyPurchaseDrawer.setDisable(false);
    }
    public void propertyPurchaseDrawerDisable() {
        propertyPurchaseDrawer.setDisable(true);
        propertyCardPurchaseAnchorPane.setVisible(false);
    }

    public void propertyPurchaseDrawerPurchaseButtonOnClick() {
        propertyPurchaseDrawer.close();
        game.setPropertyPurchaseDecision(true);
    }

    public void propertyPurchaseDrawerDeclineButtonOnClick() {
        propertyPurchaseDrawer.close();
        game.setPropertyPurchaseDecision(false);
    }



    // ============================================  BOARD  ==============================================

    public void movePlayer(String playerId, int tileIndex, boolean moveBackwards, boolean toJail) {
        Circle playerPiece = playerPiecesMap.get(playerId);
        Platform.runLater(() -> {
            propertyPurchaseDrawer.close();
            boardPane.getChildren().remove(playerPiece);
            ((VBox) jailTile.getChildren().get(0)).getChildren().remove(playerPiece);
            ((VBox) jailTile.getChildren().get(1)).getChildren().remove(playerPiece);
            if (tileIndex == 10 && toJail) ((VBox) jailTile.getChildren().get(1)).getChildren().add(playerPiece);
            else if (tileIndex == 10) ((VBox) jailTile.getChildren().get(0)).getChildren().add(playerPiece);
            else boardPane.add(playerPiece, tileIndexToCoordinatesMapper.get(tileIndex)[1], tileIndexToCoordinatesMapper.get(tileIndex)[0]);
        });
    }

    public void propertySetOwnership(String playerId, int propertyTileIndex) {
        Platform.runLater(() -> {
            if (playerId.equals("")) {
                boardPropertyTileOwnershipRectanglesMap.get(propertyTileIndex).setVisible(false);
            } else {
                Paint ownerPaint = playerPiecesMap.get(playerId).getFill();
                boardPropertyTileOwnershipRectanglesMap.get(propertyTileIndex).setVisible(true);
                boardPropertyTileOwnershipRectanglesMap.get(propertyTileIndex).setFill(ownerPaint);
            }
        });
    }

    public void propertySetMortgagedStatus(int propertyTileIndex, String playerId, boolean isMortgaged) {
        Color ownerColor = (Color) playerPiecesMap.get(playerId).getFill();
        Stop[] stops;
        double startX;
        double startY;
        double endX;
        if (propertyTileIndex < 10 || propertyTileIndex > 20 && propertyTileIndex < 30) {
            startX = 0;
            startY = 0;
            endX = 1;
        } else {
            startX = 1;
            startY = 0;
            endX = 0;
        }
        if (propertyTileIndex < 20) {
            stops = new Stop[] {new Stop(0, BLACK), new Stop(0.4, BLACK), new Stop(0.6, ownerColor), new Stop(1, ownerColor), };
        } else {
            stops = new Stop[] {new Stop(0, ownerColor), new Stop(0.4, ownerColor), new Stop(0.6, BLACK), new Stop(1, BLACK), };
        }

        LinearGradient linearGradient = new LinearGradient(startX, startY, endX, 1, true, CycleMethod.NO_CYCLE, stops);

        Platform.runLater(() -> {
            boardPropertyTileOwnershipRectanglesMap.get(propertyTileIndex).setVisible(true);
            if (isMortgaged) {
                boardPropertyTileOwnershipRectanglesMap.get(propertyTileIndex).setFill(linearGradient);
            } else {
                boardPropertyTileOwnershipRectanglesMap.get(propertyTileIndex).setFill(ownerColor);
            }
        });
    }


    private void matchPropertyTileCardToCorrectBackground(ImageView imageview, PropertyTile propertyTile) {
        if (propertyTile instanceof RegularPropertyTile) {
            imageview.setImage(new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/propertyTileCards/propertyTileCardRegularProperty.png")));
        } else if (propertyTile instanceof RailroadPropertyTile) {
            imageview.setImage(new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/propertyTileCards/propertyTileCardRailroad.png")));
        } else {
            if (propertyTile.getPosition() == 12) {
                imageview.setImage(new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/propertyTileCards/propertyTileCardUtilityFirst.png")));
            } else {
                imageview.setImage(new Image(getClass().getResourceAsStream("/ee/taltech/iti0200/monopoly/game/images/propertyTileCards/propertyTileCardUtilitySecond.png")));
            }

        }
    }

    private Paint matchRegularPropertyTileCardColorToColorCode(PropertyColor propertyColor) {
        if (propertyColor.equals(PropertyColor.BROWN)) return Color.valueOf("#9a5b25");
        if (propertyColor.equals(PropertyColor.LIGHT_BLUE)) return Color.valueOf("#87a5d6");
        if (propertyColor.equals(PropertyColor.MAGENTA)) return Color.valueOf("#e3097e");
        if (propertyColor.equals(PropertyColor.ORANGE)) return Color.valueOf("#f57f23");
        if (propertyColor.equals(PropertyColor.RED)) return Color.valueOf("#da2327");
        if (propertyColor.equals(PropertyColor.YELLOW)) return Color.valueOf("#fde703");
        if (propertyColor.equals(PropertyColor.GREEN)) return Color.valueOf("#13a55b");
        if (propertyColor.equals(PropertyColor.DARK_BLUE)) return Color.valueOf("#274ea1");
        throw new RuntimeException(String.format("Error occurred while trying to match propertyColor '%s' to a color code.", propertyColor));
    }

    // ============================================  CARDS  ==============================================

    public void showTakeACard(String cardTitle, String cardTextRow1, String cardTextRow2, String cardTextRow3, String cardTextRow4, boolean showSwitchButton, boolean showOkButton, boolean showPayButton, String takerPlayerName) {
        Platform.runLater(() -> {
            cardTitleLabel.textProperty().setValue(cardTitle);
            cardPlayerNameLabel.textProperty().setValue(takerPlayerName + ",");
            cardTextRow1Label.textProperty().setValue(cardTextRow1);
            cardTextRow2Label.textProperty().setValue(cardTextRow2);
            cardTextRow3Label.textProperty().setValue(cardTextRow3);
            cardTextRow4Label.textProperty().setValue(cardTextRow4);
            cardChoiceSwitchImageView.setVisible(showSwitchButton);
            cardChoicePayImageView.setVisible(showPayButton);
            cardOkImageView.setVisible(showOkButton);
            cardAnchorPane.setVisible(true);
        });
    }
    // ======================================  CARDS ON-CLICKS =====================================

    private void cardViewHide() {
        cardChoiceSwitchImageView.setVisible(false);
        cardChoicePayImageView.setVisible(false);
        cardOkImageView.setVisible(false);
        cardAnchorPane.setVisible(false);
    }


    // ==================================== PLAYER INFO PANELS ON THE LEFT ============================================

    public void showPlayerTurn(String playerId, boolean isThisClient) {
        Platform.runLater(() -> {
            for (AnchorPane anchorPane: playerInfoAnchorPanes.values()) {
                anchorPane.setEffect(null);
            }
            playerInfoAnchorPanes.get(playerId).setEffect(innerShadow);
            if (isThisClient) audioClip.play();
            propertyPurchaseDrawer.close();
            cardViewHide();
        });
    }

    // ============================================ PLAYER INFO BALANCE ================================================

    public void showChangePlayerBalance(String payerPlayerId, double payerPlayerNewBalance, String receiverPlayerId, double receiverPlayerNewBalance, double transferredAmount, boolean fromBank, boolean toBank) {
        if (fromBank) {
            showIncreasePlayerBalanceFromBank(((Label)playerInfoBalanceVBoxesMap.get(receiverPlayerId).getChildren().get(1)), receiverPlayerId, receiverPlayerNewBalance, transferredAmount);
        } else if (toBank) {
            showDecreasePlayerBalanceToBank(((Label)playerInfoBalanceVBoxesMap.get(payerPlayerId).getChildren().get(1)), payerPlayerId, payerPlayerNewBalance, transferredAmount);
        } else {
            showIncreasePlayerBalanceFromAnotherPlayer(((Label)playerInfoBalanceVBoxesMap.get(receiverPlayerId).getChildren().get(1)), payerPlayerId, payerPlayerNewBalance, receiverPlayerId, receiverPlayerNewBalance, transferredAmount);
            showDecreasePlayerBalanceToAnotherPlayer(((Label)playerInfoBalanceVBoxesMap.get(payerPlayerId).getChildren().get(1)), payerPlayerId, payerPlayerNewBalance, receiverPlayerId, receiverPlayerNewBalance, transferredAmount);
        }
    }

    private void showIncreasePlayerBalanceFromBank(Label balanceLabel, String receiverPlayerId, double receiverPlayerNewBalance, double transferredAmount) {
        Label playerInfoBalanceChangeIncreaseLabel = (Label)playerInfoBalanceVBoxesMap.get(receiverPlayerId).getChildren().get(0);
        showBalanceChangeFadeTransition(balanceLabel, decimalFormat.format(receiverPlayerNewBalance), playerInfoBalanceChangeIncreaseLabel, ("+ " + decimalFormat.format(transferredAmount)), BLACK, fadeInOutBalanceIncreaseTransitionsMap.get(receiverPlayerId));
    }

    private void showDecreasePlayerBalanceToBank(Label balanceLabel, String payerPlayerId, double payerPlayerNewBalance, double transferredAmount) {
        Label playerInfoBalanceChangeDecreaseLabel = (Label)playerInfoBalanceVBoxesMap.get(payerPlayerId).getChildren().get(2);
        showBalanceChangeFadeTransition(balanceLabel, decimalFormat.format(payerPlayerNewBalance), playerInfoBalanceChangeDecreaseLabel, ("- " + decimalFormat.format(transferredAmount)), BLACK, fadeInOutBalanceDecreaseTransitionsMap.get(payerPlayerId));
    }

    private void showIncreasePlayerBalanceFromAnotherPlayer(Label balanceLabel, String payerPlayerId, double payerPlayerNewBalance, String receiverPlayerId, double receiverPlayerNewBalance, double transferredAmount) {
        Color payerPlayerColor = (Color) playerPiecesMap.get(payerPlayerId).getFill();
        Label playerInfoBalanceChangeIncreaseLabel = (Label)playerInfoBalanceVBoxesMap.get(receiverPlayerId).getChildren().get(0);
        showBalanceChangeFadeTransition(balanceLabel, decimalFormat.format(receiverPlayerNewBalance), playerInfoBalanceChangeIncreaseLabel, ("+ " + decimalFormat.format(transferredAmount)), payerPlayerColor, fadeInOutBalanceIncreaseTransitionsMap.get(receiverPlayerId));
    }

    private void showDecreasePlayerBalanceToAnotherPlayer(Label balanceLabel, String payerPlayerId, double payerPlayerNewBalance, String receiverPlayerId, double receiverPlayerNewBalance, double transferredAmount) {
        Color receiverPlayerColor = (Color) playerPiecesMap.get(receiverPlayerId).getFill();
        Label playerInfoBalanceChangeDecreaseLabel = (Label)playerInfoBalanceVBoxesMap.get(payerPlayerId).getChildren().get(2);
        showBalanceChangeFadeTransition(balanceLabel, decimalFormat.format(payerPlayerNewBalance), playerInfoBalanceChangeDecreaseLabel, ("- " + decimalFormat.format(transferredAmount)), receiverPlayerColor, fadeInOutBalanceDecreaseTransitionsMap.get(payerPlayerId));
    }

    private void showBalanceChangeFadeTransition(Label balanceLabel, String balanceLabelText , Label increaseDeductLabel, String increaseDeductLabelText, Color increaseDeductLabelColor, FadeTransition fadeTransition) {
        Platform.runLater(() -> {
            balanceLabel.textProperty().set(balanceLabelText);

            increaseDeductLabel.setTextFill(increaseDeductLabelColor);
            increaseDeductLabel.textProperty().setValue(increaseDeductLabelText);
            fadeTransition.setNode(increaseDeductLabel);
            fadeTransition.setFromValue(0.0);
            fadeTransition.setToValue(1.0);
            fadeTransition.setCycleCount(2);
            fadeTransition.setAutoReverse(true);
            fadeTransition.playFromStart();
        });
    }

    // ====================================  SHOW PLAYER GET OUT OF JAIL OPTIONS  ======================================

    public void givePlayerGetOutOfJailOptions(Player player, boolean allowRollingTheDice, boolean allowPaying, boolean allowUsingACard) {
        List<Boolean> listOfAllowedActions = new ArrayList<>(List.of(allowRollingTheDice, allowPaying, allowUsingACard));
        List<ImageView> actionButtons = List.of(diceRollImageViewWhite, payOutOfJailImageViewWhite, getOutOfJailCardImageViewWhite);
        for (ImageView actionButton: actionButtons) {
            actionButton.setOpacity(1);
            actionButton.setDisable(false);
        }
        for (int i = 0; i < 3; i++) {
            if (!listOfAllowedActions.get(i)) {
                ImageView tempImageView = actionButtons.get(i);
                tempImageView.setDisable(true);
                tempImageView.setOpacity(0.3);
            }
        }
        getOutOfJailOptionsAnchorPane.setVisible(true);
    }

    // ====================================  BANKRUPTCY  ======================================

    public void disablePlayerPieceAndRectangle(String playerId, int playerIndex) {
        Platform.runLater(() -> {
            playerPiecesMap.get(playerId).setDisable(true);
            playerPiecesMap.get(playerId).setVisible(false);
            playersInfoVBox.getChildren().get(playerIndex).setDisable(true);
        });
    }

    public void showBankruptcyWarning() {
        bankruptcyWarningAnchorPane.setVisible(true);
    }

    public void showWinnerScreen(String playerName) {
        Platform.runLater(() -> {
            winnerLabel.textProperty().setValue(String.format("Võitja on %s.", playerName));
            winnerAnchorPane.setVisible(true);
        });
    }
}
