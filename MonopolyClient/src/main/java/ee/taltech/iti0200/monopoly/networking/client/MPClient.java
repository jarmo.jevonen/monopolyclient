package ee.taltech.iti0200.monopoly.networking.client;

import com.esotericsoftware.kryonet.Client;
import ee.taltech.iti0200.monopoly.networking.packets.Packets;
import lombok.Getter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MPClient {

    private int serverPort;
    private String serverIp;
    @Getter
    private String userName;
    @Getter
    private String playerId;
    @Getter
    protected static final Logger clientLogger = Logger.getLogger(MPClient.class.getName());
    private static final String CLIENT_LOGGER_PREFIX = "ClientLog-> ";

    @Getter
    private Client client;
    private ClientNetworkListener cnl;
    @Getter
    private GameNetworkingClientLogic gameNetworkingClientLogic;

    public MPClient(String ipAddress, int port, String userName, GameNetworkingClientLogic gameNetworkingClientLogic, String lobbyId, boolean joinLobby) {
        clientLogger.setLevel(Level.WARNING);
        setUp(ipAddress, port, userName);
        connect();
        this.gameNetworkingClientLogic = gameNetworkingClientLogic;
        if (joinLobby) {
            joinLobby(lobbyId);
        } else {
            createLobby();
        }
    }

    private void setUp(String ipAddress, int port, String userName) {
        clientLogger.log(Level.INFO, CLIENT_LOGGER_PREFIX + "Setting up a client.");
        serverIp = ipAddress;
        serverPort = port;
        this.userName = userName;
        this.playerId = "";
        client = new Client(16384, 16384);
        client.setKeepAliveTCP(20000);
        client.setTimeout(25000);
        cnl = new ClientNetworkListener(this);
        Packets.RegisterPackets(client.getKryo());
        client.addListener(cnl);
        new Thread(client).start();
    }

    private void connect() {
        clientLogger.log(Level.INFO, String.format("ClientLogger-> Connecting to the server: '%s' at port: '%d'", serverIp, serverPort));
        try {
            client.connect(5000, serverIp, serverPort);
        } catch (IOException e) {
            clientLogger.log(Level.WARNING, String.format("ClientLogger-> Unable to connect to server '%s' at port '%d'", serverIp, serverPort));
        }
    }

    public void createLobby() {
        Packets.PacketServerReceiveCreateLobby packetServerReceiveCreateLobby = new Packets.PacketServerReceiveCreateLobby();
        client.sendTCP(packetServerReceiveCreateLobby);
    }

    public void joinLobby(String lobbyId) {
        gameNetworkingClientLogic.setLobbyId(lobbyId);
        gameNetworkingClientLogic.receiveLobbyId(lobbyId);
        Packets.PacketRegisterPlayerFromClient packetRegisterPlayerFromClient = new Packets.PacketRegisterPlayerFromClient();
        packetRegisterPlayerFromClient.setPlayerName(userName);
        packetRegisterPlayerFromClient.setLobbyId(lobbyId);
        client.sendTCP(packetRegisterPlayerFromClient);
    }

    // Called by ClientNetworkListener to set a server-assigned ID for the player.
    protected void assignPlayerId(String playerId) {
        clientLogger.log(Level.INFO, String.format("ClientLogger-> Client with name '%s' has been assigned an id: '%s'", userName, playerId));
        this.playerId = playerId;
    }
}
