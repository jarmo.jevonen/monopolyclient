package ee.taltech.iti0200.monopoly.networking.client;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import ee.taltech.iti0200.monopoly.networking.packets.Packets;

import java.io.IOException;
import java.util.logging.Level;

public class ClientNetworkListener extends Listener {
    private static final String cnlLoggerPrefix = "CnlLog-> ";

    private MPClient mpClient;

    public ClientNetworkListener(MPClient mpClient) {
        this.mpClient = mpClient;
    }

    public void connected(Connection connection) {
        MPClient.clientLogger.log(Level.INFO,  cnlLoggerPrefix + "Connected to the server with connection ID: " + connection.getID());
        connection.setKeepAliveTCP(20000);
        connection.setTimeout(25000);
    }

    public void disconnected(Connection connection) {
        MPClient.clientLogger.log(Level.INFO, String.format("%sConnection with ID: %d disconnected at: %s", cnlLoggerPrefix, connection.getID(), System.currentTimeMillis()));
        mpClient.getGameNetworkingClientLogic().receiveClientDisconnected();
    }

    public void received(Connection connection, Object object) {
        // Dummy packet for simple string-based messages.
        if (object instanceof Packets.PacketSimpleMessage) {
            Packets.PacketSimpleMessage packet = (Packets.PacketSimpleMessage) object;
            MPClient.clientLogger.log(Level.INFO, cnlLoggerPrefix + "Response from the server: " + packet.getChatMessage());
        }

        if (object instanceof Packets.PacketServerSendLobbyCreated) {
            Packets.PacketServerSendLobbyCreated packet = (Packets.PacketServerSendLobbyCreated) object;
            mpClient.joinLobby(packet.getLobbyId());
        }


        // Packet from the server providing an ID for the connected client.
        else if (object instanceof Packets.PacketRegisterPlayerFromServer) {
            Packets.PacketRegisterPlayerFromServer packet = (Packets.PacketRegisterPlayerFromServer) object;
            mpClient.assignPlayerId(packet.getPlayerId());
        }
        // Packet from the server announcing clients' ready-statuses in the main menu.
        else if (object instanceof Packets.PacketServerSendPlayerReadyObjectsJson) {
            Packets.PacketServerSendPlayerReadyObjectsJson packet = (Packets.PacketServerSendPlayerReadyObjectsJson) object;
            mpClient.getGameNetworkingClientLogic().getDtoGameDTOHandler().receiveDTOPlayerReadyStatusesJsonString(packet.getPlayerReadyObjectsJsonString());
        }
        // Packet from the server announcing game start.
        else if (object instanceof Packets.PacketServerSendGameStart) {
            Packets.PacketServerSendGameStart packet = (Packets.PacketServerSendGameStart) object;
            mpClient.getGameNetworkingClientLogic().receiveStartTheGame();
        }
        // Packet from the server containing DTOGameboardData
        else if (object instanceof Packets.PacketServerSendDTOGameboardData) {
            Packets.PacketServerSendDTOGameboardData packet = (Packets.PacketServerSendDTOGameboardData) object;
            try {
                mpClient.getGameNetworkingClientLogic().getDtoGameDTOHandler().receiveDTOGameDataJsonStrings(packet.getTypeOfDTOGameboardData(), packet.getDTOGameboardDataJsonString());
            } catch (IOException e) {
                MPClient.clientLogger.log(Level.WARNING,  cnlLoggerPrefix + "Unable to parse DTOgameboardData: " + e);
            }
        }
        // Packet from the server containing DTOGameplayAction
        else if (object instanceof Packets.PacketServerSendDTOGameplayAction) {
            Packets.PacketServerSendDTOGameplayAction packet = (Packets.PacketServerSendDTOGameplayAction) object;
            try {
                mpClient.getGameNetworkingClientLogic().getDtoGameDTOHandler().receiveDTOGameplayJsonStrings(packet.getTypeOfDTOGameplayAction(), packet.getDTOGameplayActionJsonString());
            } catch (IOException e) {
                MPClient.clientLogger.log(Level.WARNING,  cnlLoggerPrefix + "Unable to parse DTOgameplay Action: " + e);
            }
        }

    }
}