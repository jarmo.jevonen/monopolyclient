package ee.taltech.iti0200.monopoly.game.board.tile;

public enum SpecialTileType {
    GO,
    CHANCE,
    COMMUNITY_CHEST,
    JAIL,
    FREE_PARKING,
    GO_TO_JAIL

}
