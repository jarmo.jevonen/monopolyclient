# TalTech Monopoly

ITI0200 aine projekt

Info projekti [Wikis](https://gitlab.com/jarmo.jevonen/monopolyclient/-/wikis/Kuidas-m%C3%A4ngida).

Serveri repo asub [siin](https://gitlab.com/jarmo.jevonen/monopoltserver).

## Mängu kirjeldus

Taltech Monopoly on klassikaline Monopoly, mille tegevus toimub Taltechi linnakus.

Mängu eesmärk on koguda võimalikult palju pelmeene, tehes tehinguid erinevatel objektidel ülikooli linnakus.
